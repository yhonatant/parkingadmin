package com.parkinglot.parkingadmin.view;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Comandos.ComandoConvenio;
import com.parkinglot.parkingadmin.model.Comandos.ComandoListaVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;

import org.joda.time.Instant;


import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SalidaVehiculo extends Activity implements ComandoConvenio.OnValidarListaVehiculoParqueadoChangeListener,
        ComandoListaVehiculoParqueado.OnValidarListaVehiculoParqueadoChangeListener {

    Modelo modelo = Modelo.getInstance();
    String key="";
    Utility utility;

    TextView txtplaca,txtfecha,txttiempo,txtmonto;
    Button tarjeta,efectivo,voleta,factura;
    Switch convenio;
    LinearLayout layoutconvenio;
    Button btn_convenio;
    Boolean switchState;
    ComandoConvenio comandoConvenio;
    SweetAlertDialog pDialog;
    String[] miarray;
    int inum;
    String option2;
    int numero;
    String option;
    String formaDePago="";
    String resconvenio="";
    ComandoListaVehiculoParqueado  comandoListaVehiculoParqueado;

    double cobrar;
    Long valorFraccion;
    double res;
    String moneda;
    long timestampfin;
    double _minutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView( R.layout.activity_salida_vehiculo );



        txtplaca = (TextView) findViewById( R.id.txtplaca );
        txtfecha = (TextView) findViewById( R.id.fecha );
        txttiempo = (TextView) findViewById( R.id.tiempo );
        txtmonto = (TextView) findViewById( R.id.monto );

        tarjeta = (Button) findViewById( R.id.tarjeta );
        efectivo = (Button) findViewById( R.id.efectivo );
        voleta = (Button) findViewById( R.id.voleta );
        factura = (Button) findViewById( R.id.factura );

        layoutconvenio = (LinearLayout)findViewById( R.id.layoutconvenio );

        convenio = (Switch) findViewById( R.id.switch1);
        btn_convenio = (Button) findViewById( R.id.btn_convenio);

        utility = new Utility();
        comandoConvenio = new ComandoConvenio(this);
        comandoListaVehiculoParqueado = new ComandoListaVehiculoParqueado( this );

        Bundle parametros = this.getIntent().getExtras();
        key = parametros.getString("key");

        if( key != null && !key.equals( "" )){
            Log.v("vehiculoParqueado", key);
            //Toast.makeText( getApplicationContext(), "-- " + key, Toast.LENGTH_LONG ).show();

            /*String[] parts = modelo.vehiculoParqueado.getFecha().trim().split(" ");
            String part1 = parts[0]; // 123
            String part2 = parts[1];
            String part3 = parts[2];
            String part4 = parts[3];
*/
             moneda =  modelo.parqueadero.getTipoMoneda();
            Long timestampInicio = modelo.vehiculoParqueado.getTimestamp();
            double precio = modelo.parqueadero.getPrecio();

            long timestampIncio=  timestampInicio;
            Instant instant = Instant.now();
             timestampfin = instant.getMillis();

            Log.v("timestamp",timestampIncio+"----"+timestampfin);

            Long  tiempo =(timestampfin-timestampIncio);

            Log.v( "res:" , ""+tiempo );
              /*  modelo.VehiculoParqueado = listaVehiculoParqueado.get(position);
                Intent i = new Intent(context, DetalleVehiculoParqueado.class);
                i.putExtra("VehiculoParqueado","VehiculoParqueado");
                context.startActivity(i);*/

            long minutes = (tiempo / 1000)  / 60;
            String _tiempo =  modelo.parqueadero.getformaDeCobro();

            String[] parts = _tiempo.split(" ");
            String part1 = parts[0]; // 123
            String part2 = parts[1];

            //valor fraccion
             valorFraccion = Long.parseLong(part1);

             _minutes = Double.parseDouble( ""+minutes );
            double _min = Double.parseDouble( ""+valorFraccion );

             res = (_minutes/_min);
            Log.v( "res:" , ""+res );
            double total = (res * precio);
            Log.v( "total:" , ""+total );
            int _cobrar = (int) Math.round(total);
             cobrar = Double.parseDouble( ""+_cobrar );
            Log.v( "roundDbl:" , ""+cobrar + " "+moneda);
            String pagar = cobrar + " "+moneda;


            txtplaca.setText(modelo.vehiculoParqueado.getPlaca());
            txtfecha.setText(modelo.vehiculoParqueado.getFecha());

             inum=(int) _minutes;
            String tiempoTotal = utility.formatearMinutosAHoraMinuto(inum);

            txttiempo.setText("Tiempo: "+tiempoTotal +" Minutos");
            txtmonto.setText("Cobrar "+cobrar + " "+moneda);

        }
        convenio.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {


                switchState = convenio.isChecked ();

                if(switchState){
                    layoutconvenio.setVisibility(View.VISIBLE);

                    if(utility.estado(getApplicationContext())){
                        loadswet( "Obteniendo convenios..." );
                        comandoConvenio.getListaConvenio();

                    }else{
                        alerta( "Sin Internet", "Valide la conexión a internet" );
                    }


                }else {
                    String tiempoTotal = utility.formatearMinutosAHoraMinuto(inum);

                    txttiempo.setText("Tiempo: "+tiempoTotal +" Minutos");
                   
                    layoutconvenio.setVisibility(View.GONE);
                }

               // Toast.makeText(getApplicationContext(), ":X"+switchState, Toast.LENGTH_LONG).show();
            }
        });


        btn_convenio.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                miarray = new String[modelo.listConvenio.size()];
               // String[] listItems = {"one", "two", "three", "four", "five"};

                for (int j = 0; j < modelo.listConvenio.size(); j++) {

                    // Assign each value to String array
                    miarray[j] = modelo.listConvenio.get(j).getNombre();
                }


                AlertDialog.Builder builder = new AlertDialog.Builder(SalidaVehiculo.this);
                builder.setTitle("Seleccione un convenio");
                int checkedItem = 0; //this will checked the item when user open the dialog
                builder.setSingleChoiceItems(miarray, checkedItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(SalidaVehiculo.this, "Position: " + which + " Value: " + miarray[which], Toast.LENGTH_LONG).show();

                        option =miarray[which];

                        String[] arrSplit = option.split(" ");
                        String option1 =arrSplit[0].trim();
                         option2 =arrSplit[1].trim();

                         numero = Integer.parseInt(option1);
                        numero *= -1;

                    }
                });

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                        if(option2.equals("minutos")){
                            int restarTiempo = inum - numero;
                            String tiempoTotal = utility.formatearMinutosAHoraMinuto(restarTiempo);
                            txttiempo.setText("Tiempo: "+tiempoTotal +" Minutos");
                        }


                        if(option2.equals("horas")){
                            int totalminutos = numero*60;
                            int restarTiempo = inum - totalminutos;
                            String tiempoTotal = utility.formatearMinutosAHoraMinuto(restarTiempo);
                            txttiempo.setText("Tiempo: "+tiempoTotal +" Minutos");
                        }
                        btn_convenio.setText(""+ option);
                        resconvenio = option;
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        } );
    }


    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog( SalidaVehiculo.this, SweetAlertDialog.PROGRESS_TYPE );
            pDialog.getProgressHelper().setBarColor( Color.parseColor( "#A5DC86" ) );
            pDialog.setTitleText( text );
            pDialog.setCancelable( false );
            pDialog.show();

        } catch (Exception e) {

        }

    }
    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    //alerta
    public void alerta(String titulo, String decripcion) {
        new SweetAlertDialog(SalidaVehiculo.this, SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                } )

                .show();
    }



    @Override
    public void validandoConvenioOK() {

        hideDialog();
    }

    @Override
    public void validandConvenioError() {
        hideDialog();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            atras();
            return true;
        }

        return false;
    }


    @SuppressLint("ResourceAsColor")
    public void tarjeta(View v){
        formaDePago="tarjeta";
        tarjeta.setBackgroundResource(R.drawable.edittext_blue_style);
        tarjeta.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        efectivo.setBackgroundResource(R.drawable.edittext_style);
        efectivo.setTextColor( getResources().getColor(R.color.colorApp));

        voleta.setBackgroundResource(R.drawable.edittext_style);
        voleta.setTextColor( getResources().getColor(R.color.colorApp));

        factura.setBackgroundResource(R.drawable.edittext_style);
        factura.setTextColor( getResources().getColor(R.color.colorApp));

    }

    public void efectivo(View v){
        formaDePago="efectivo";

        tarjeta.setBackgroundResource(R.drawable.edittext_style);
        tarjeta.setTextColor( getResources().getColor(R.color.colorApp));

        efectivo.setBackgroundResource(R.drawable.edittext_blue_style);
        efectivo.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        voleta.setBackgroundResource(R.drawable.edittext_style);
        voleta.setTextColor( getResources().getColor(R.color.colorApp));

        factura.setBackgroundResource(R.drawable.edittext_style);
        factura.setTextColor( getResources().getColor(R.color.colorApp));

    }

    public void voleta(View v){
        formaDePago="voleta";

        tarjeta.setBackgroundResource(R.drawable.edittext_style);
        tarjeta.setTextColor( getResources().getColor(R.color.colorApp));

        efectivo.setBackgroundResource(R.drawable.edittext_style);
        efectivo.setTextColor( getResources().getColor(R.color.colorApp));

        voleta.setBackgroundResource(R.drawable.edittext_blue_style);
        voleta.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        factura.setBackgroundResource(R.drawable.edittext_style);
        factura.setTextColor( getResources().getColor(R.color.colorApp));
    }

    public void factura(View v){
        formaDePago="factura";

        tarjeta.setBackgroundResource(R.drawable.edittext_style);
        tarjeta.setTextColor( getResources().getColor(R.color.colorApp));

        efectivo.setBackgroundResource(R.drawable.edittext_style);
        efectivo.setTextColor( getResources().getColor(R.color.colorApp));

        voleta.setBackgroundResource(R.drawable.edittext_style);
        voleta.setTextColor( getResources().getColor(R.color.colorApp));

        factura.setBackgroundResource(R.drawable.edittext_blue_style);
        factura.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

    }

    public void registrarpago(View v){

        if(formaDePago.equals("")){
            alerta("Forma de pago", "Selecione la forma de pago");
        }else {
            generarPago();

        }

    }


    //alerta
    public void generarPago(){



        //fecha y hora actual
        Date date = new Date();
        String fechacComplString = modelo.dateFormat.format(date);
        String horaString = modelo.hourFormat.format(date);

        String fechaActual = fechacComplString + " " + horaString;

        new SweetAlertDialog(SalidaVehiculo.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Valor a cobrar")
                .setContentText(cobrar+" "+ moneda + " " + txtplaca.getText().toString())
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        if (utility.estado(SalidaVehiculo.this)) {
                            comandoListaVehiculoParqueado.updateListaVhiculoParqueado( cobrar,  valorFraccion, res, moneda,  txtplaca.getText().toString(),   timestampfin,  _minutes,key,formaDePago,resconvenio,fechaActual );
                        }else {
                            alerta("Sin Internet","Valide la conexión a internet");
                        }

                        sDialog.dismissWithAnimation();

                    }
                })
                .setCancelButton("Cancelar", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })

                .show();
    }

    @Override
    public void validandoListaVehiculoParqueadoOK() {
        atras();
    }

    @Override
    public void validandoListaVehiculoParqueadoError() {
      atras();
    }

    public void  atras(){
        Intent i = new Intent(getApplicationContext(), Inicio.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();
    }
}