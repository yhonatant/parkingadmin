package com.parkinglot.parkingadmin.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.parkinglot.parkingadmin.MainActivity;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.Parqueadero;
import com.parkinglot.parkingadmin.model.Clases.Timedialog;
import com.parkinglot.parkingadmin.model.Clases.Usuario;
import com.parkinglot.parkingadmin.model.Comandos.ComandoParqueadero;
import com.parkinglot.parkingadmin.model.Comandos.ComandoValidarCorreoFirebase;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;
import java.util.regex.Pattern;
import cn.pedant.SweetAlert.SweetAlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;


import androidx.annotation.NonNull;

import java.util.Calendar;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;



public class Registro extends Activity implements ComandoParqueadero.OnParqueaderosChangeListener, ComandoValidarCorreoFirebase.OnValidarCorreoFirebaseChangeListener ,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    TextView title;
    EditText name, mobile, lastname, email, password, comfirmpassword;
    EditText nameparkinlot, starttime, endttime, formofpayment, price, quotas, currency;
    Button next, save, back;
    Modelo modelo = Modelo.getInstance();
    String token = "";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private static final String TAG = "AndroidBash";
    Utility utility;
    SweetAlertDialog pDialog;
    Parqueadero parqueadero;
    LinearLayout datauser, dataparqueadero;
    ComandoParqueadero comandoParqueadero;
    ComandoValidarCorreoFirebase comandoValidarCorreoFirebase;
    FragmentManager childFragmentManager = getFragmentManager();
    int setHora = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        setContentView( R.layout.activity_registro );


        if (savedInstanceState != null) {
            Intent i = new Intent( getApplicationContext(), MainActivity.class );
            startActivity( i );
            finish();
            return;
        }



        token = FirebaseInstanceId.getInstance().getToken();
        modelo.token = token;

        comandoParqueadero = new ComandoParqueadero( this );
        comandoValidarCorreoFirebase = new ComandoValidarCorreoFirebase( this );

        utility = new Utility();
        parqueadero = null;
        boolean result = Utility.checkPermission( Registro.this );

        datauser = (LinearLayout) findViewById( R.id.datauser );
        dataparqueadero = (LinearLayout) findViewById( R.id.dataparqueadero );
        title = (TextView) findViewById( R.id.title );
        name = (EditText) findViewById( R.id.name );
        mobile = (EditText) findViewById( R.id.mobile );
        lastname = (EditText) findViewById( R.id.lastname );
        email = (EditText) findViewById( R.id.email );
        password = (EditText) findViewById( R.id.password );
        comfirmpassword = (EditText) findViewById( R.id.comfirmpassword );
        nameparkinlot = (EditText) findViewById( R.id.nameparkinlot );
        starttime = (EditText) findViewById( R.id.starttime );
        endttime = (EditText) findViewById( R.id.endttime );
        formofpayment = (EditText) findViewById( R.id.formofpayment );
        price = (EditText) findViewById( R.id.price );
        quotas = (EditText) findViewById( R.id.quotas );
        currency = (EditText) findViewById( R.id.currency );
        next = (Button) findViewById( R.id.next );
        back = (Button) findViewById( R.id.back );
        save = (Button) findViewById( R.id.save );

        Log.d("Firebase", "token "+ FirebaseInstanceId.getInstance().getToken());

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Obtenemos el token
                        String token = task.getResult().getToken();
                        modelo.token = token;

                        // Mostramos el token
                        Toast.makeText(Registro.this, token, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void next(View v) {

        if (name.getText().toString().length() < 3) {
            name.setError( "nombre muy corta" );
        } else if (mobile.getText().toString().length() < 3) {
            mobile.setError( "celular muy corto" );
        } else if (lastname.getText().toString().length() < 3) {
            lastname.setError( "apellido muy corto" );
        } else if (email.getText().toString().length() < 3) {
            email.setError( "correo muy corto" );
        } else if (email.getText().toString().length() < 5) {
            email.setError( "Email muy corta" );
        } else if (!validarEmail( email.getText().toString() )) {
            email.setError( "Email no válido" );
        } else if (password.getText().toString().length() < 8) {
            password.setError( "Contraseña muy corta, minimo 7 caracteres" );
        } else if (comfirmpassword.getText().toString().length() < 8) {
            comfirmpassword.setError( "Contraseña muy corta, minimo 7 caracteres" );
        } else if (!password.getText().toString().equals( comfirmpassword.getText().toString() )) {
            password.setError( "Las contraseña no coinciden." );
        } else {
            next.setVisibility( View.GONE );
            datauser.setVisibility( View.GONE );
            dataparqueadero.setVisibility( View.VISIBLE );
            title.setText( "Datos Parqueadero" );
            Animation myanim = AnimationUtils.loadAnimation( this, R.anim.splashanimation );
            dataparqueadero.startAnimation( myanim );
            back.setVisibility( View.VISIBLE );
            save.setVisibility( View.VISIBLE );
        }
    }

    public void back(View v) {
        Animation myanim = AnimationUtils.loadAnimation( this, R.anim.splashanimation );
        next.setVisibility( View.VISIBLE );
        back.setVisibility( View.GONE );
        save.setVisibility( View.GONE );
        datauser.setVisibility( View.VISIBLE );
        dataparqueadero.setVisibility( View.GONE );
        title.setText( "Datos Usuario" );
        datauser.startAnimation( myanim );
    }

    public void save(View v) {


        if (nameparkinlot.getText().toString().length() < 3) {
            nameparkinlot.setError( "nombre del parqueadero muy corta" );
        } else if (starttime.getText().toString().length() < 3) {
            starttime.setError( "Fecha inicio  requerido" );
        } else if (endttime.getText().toString().length() < 3) {
            endttime.setError( "Fecha fin requerido" );
        } else if (price.getText().toString().length() < 2) {
            price.setError( "El precio es requerido" );
        } else if (currency.getText().toString().length() < 2) {
            currency.setError( "Valor moneda es requerido" );
        } else if (formofpayment.getText().toString().length() < 3) {
            formofpayment.setError( "Forma de cobro es requerido" );
        } else if (quotas.getText().toString().length() < 2) {
            quotas.setError( "Cupos es requerido" );
        } else {

            if (utility.estado( getApplicationContext() )) {
                loadswet( "Validando la información..." );
                comandoValidarCorreoFirebase.checkAccountEmailExistInFirebase( email.getText().toString() );

            } else {
                alerta( "Sin Internet", "Valide la conexión a internet" );
            }

        }
    }

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog( this, SweetAlertDialog.PROGRESS_TYPE );
            pDialog.getProgressHelper().setBarColor( Color.parseColor( "#A5DC86" ) );
            pDialog.setTitleText( text );
            pDialog.setCancelable( false );
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher( email ).matches();
    }

    //alerta swit alert
    //alerta
    public void alerta(String titulo, String decripcion) {
        new SweetAlertDialog( this, SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                } )

                .show();
    }

    //alerta
    public void alertaOK(String titulo, String decripcion) {

        new SweetAlertDialog( this, SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        Intent i = new Intent( getApplicationContext(), Login.class );
                        startActivity( i );
                        overridePendingTransition( R.anim.push_down_in, R.anim.push_down_out );

                    }
                } )

                .show();
    }

    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    @Override
    public void cargoValidarCorreoFirebase() {

        //string key,
        String nombre = name.getText().toString();
        String apellido = lastname.getText().toString();
        String celular = mobile.getText().toString();
        String correo = email.getText().toString();
        String pasString = password.getText().toString();
        String token = this.token;
        double lat = modelo.latitud;
        double lon = modelo.latitud;
        boolean estado = true;
        String rol = "adminParqueadero";

        Usuario usuario = new Usuario( "", nombre, apellido, celular, correo, pasString, token, lat, lon, estado, rol );
        comandoValidarCorreoFirebase.registroUsuario( usuario );
    }

    @Override
    public void cargoValidarCorreoFirebaseEroor() {
        email.setError( "Correo ya registrado" );
        hideDialog();
    }

    @Override
    public void setUsuarioListener() {

        String nombreParqueadero = nameparkinlot.getText().toString();
        boolean estado = true;
        double lat = modelo.latitud;
        double lon = modelo.latitud;
        String horarioInicio = starttime.getText().toString();
        String horarioFin = endttime.getText().toString();
        double precio = Double.parseDouble( price.getText().toString() );
        int cupos = Integer.parseInt( quotas.getText().toString() );
        int disponibles = Integer.parseInt( quotas.getText().toString() );
        String tipoMoneda = currency.getText().toString();
        String formaDeCobro = formofpayment.getText().toString();

        Parqueadero parqueadero = new Parqueadero( "", nombreParqueadero, estado, lat, lon, horarioInicio, horarioFin, precio, cupos, disponibles, tipoMoneda,formaDeCobro, modelo.uid );
        comandoParqueadero.registroParqueadero( parqueadero );
    }

    @Override
    public void errorSetUsuario() {
        hideDialog();
        alerta( "Error Resgisto", "Algo salió malcon el registro verifique su conexión." );
    }

    @Override
    public void errorCreacionUsuario() {
        hideDialog();

        alerta( "Error Resgisto", "Algo salió malcon el registro verifique su conexión" );
    }

    @Override
    public void setParqueaderoListener() {
        hideDialog();
        alertaOK( "Registro exitoso", "Inicie sesion" );
    }

    @Override
    public void errorSetParqueadero() {
        hideDialog();
        alerta( "Error Resgisto", "Algo salió malcon el registro verifique su conexión." );

    }

    @Override
    public void setParueaderoListener() {

    }

    @Override
    public void errorCreacionParqueadero() {
        hideDialog();
        alerta( "Error Resgisto", "Algo salió malcon el registro verifique su conexión." );

    }

    @Override
    public void cargoParqueadero() {

    }

    @Override
    public void addParueaderoListener() {

    }


    ///modal tipo moneda
    public void currency(View v) {

        final String[] singleChoiceItems = getResources().getStringArray( R.array.arraycurrency );
        int itemSelected = 0;
        new AlertDialog.Builder( this )
                .setTitle( "Selecione el tipo de moneda" )
                .setSingleChoiceItems( singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                        currency.setText( singleChoiceItems[selectedIndex] );
                    }
                } )
                .setPositiveButton( "Ok", null )
                .setNegativeButton( "Cancel", null )
                .show();
    }

    public void formadecobro(View v) {

        final String[] singleChoiceItems = getResources().getStringArray( R.array.formadecobro );
        int itemSelected = 0;
        new AlertDialog.Builder( this )
                .setTitle( "Selecione el tipo de fracción " )
                .setSingleChoiceItems( singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                        formofpayment.setText( singleChoiceItems[selectedIndex] );
                    }
                } )
                .setPositiveButton( "Ok", null )
                .setNegativeButton( "Cancel", null )
                .show();
    }


    //picker

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        starttime.setText(date);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = "You picked the following time: " + hourString + "h" + minuteString + "m" + secondString + "s";
        starttime.setText(time);
    }


    public void hora(View view) {

        switch (view.getId()) {
            case R.id.starttime:
                setHora = 1;
                customTimePickerDialog(view);
                break;

            case R.id.endttime:
                setHora = 2;
                customTimePickerDialog(view);
                break;
        }
    }

    private void customTimePickerDialog(View view) {

        Timedialog timedialog = new Timedialog();

        timedialog.get(view);
        android.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        timedialog.show(fragmentTransaction,"TimePicker");
    }

}

