package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

public class EsEntrada {

    private  String es_salida;
    private  String es_caja;
    private  int  org_id;

    public EsEntrada(){}

    public EsEntrada(String es_salida, String es_salida1, int org_id) {
        this.es_salida = es_salida;
        this.es_salida = es_salida1;
        this.org_id = org_id;
    }

    public String getEs_salida() {
        return es_salida;
    }

    public void setEs_salida(String es_salida) {
        this.es_salida = es_salida;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }
}
