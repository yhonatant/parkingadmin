package com.parkinglot.parkingadmin.Retrofit.viewRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

public class ContextoListRetrofit {


    private  String  tar_id;
    private  String  tar_nombre;
    private  String  tar_precio;
    private  String  tar_minutos;
    private  String  tar_tipo;

    public ContextoListRetrofit() {

    }

    public ContextoListRetrofit(JSONObject jsonObject) throws JSONException {
        this.tar_id = jsonObject.getString("tar_id");
        this.tar_nombre = jsonObject.getString("tar_nombre");
        this.tar_precio = jsonObject.getString("tar_precio");
        this.tar_minutos = jsonObject.getString("tar_minutos");
        this.tar_tipo = jsonObject.getString("tar_tipo");
    }

    public String getTar_id() {
        return tar_id;
    }

    public void setTar_id(String tar_id) {
        this.tar_id = tar_id;
    }

    public String getTar_nombre() {
        return tar_nombre;
    }

    public void setTar_nombre(String tar_nombre) {
        this.tar_nombre = tar_nombre;
    }

    public String getTar_precio() {
        return tar_precio;
    }

    public void setTar_precio(String tar_precio) {
        this.tar_precio = tar_precio;
    }

    public String getTar_minutos() {
        return tar_minutos;
    }

    public void setTar_minutos(String tar_minutos) {
        this.tar_minutos = tar_minutos;
    }

    public String getTar_tipo() {
        return tar_tipo;
    }

    public void setTar_tipo(String tar_tipo) {
        this.tar_tipo = tar_tipo;
    }
}
