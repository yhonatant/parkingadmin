package com.parkinglot.parkingadmin.Retrofit.viewRetrofit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.felipecsl.gifimageview.library.GifImageView;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.Contexto;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.UsuarioParqueaderoRetrofit;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.fragment.CajaFragment;
import com.parkinglot.parkingadmin.model.Modelo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleFacturaRetrofit extends Activity {

    Modelo modelo = Modelo.getInstance();
    VehiculoSalidaRetrofit vehiculoSalidaRetrofit;

    SweetAlertDialog pDialog;
    TextView idpar, placa, hora, descuento, monto,optiondescuento;

    LinearLayout option;
    Contexto contexto;
    ArrayList<ContextoListRetrofit> contextoListRetrofits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.fragment_detalle_factura_retrofit);


        String parqueoID = modelo.vehiculoSalidaRetrofits.getPar_id();
        String salida = modelo._salisa;
        Log.v("", "");

        idpar = (TextView) findViewById(R.id.idpar);
        placa = (TextView) findViewById(R.id.placa);
        descuento = (TextView) findViewById(R.id.descuento);
        optiondescuento = (TextView) findViewById(R.id.optiondescuento);
        hora = (TextView) findViewById(R.id.hora);
        monto = (TextView) findViewById(R.id.monto);
        option = (LinearLayout) findViewById(R.id.option);
        option = (LinearLayout) findViewById(R.id.option);


        idpar.setText(modelo.vehiculoSalidaRetrofits.getPar_id());
        placa.setText(modelo.vehiculoSalidaRetrofits.getPar_placa());
        hora.setText(modelo.vehiculoSalidaRetrofits.getPar_hora_ingreso());


        cargatContexto();

        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String[] optio = new String[contexto.getList_descuentos().length()];

                List<String> list = new ArrayList<String>();
                //  JSONArray arr = contexto.getList_descuentos();

                for (int i =0; i < contextoListRetrofits.size(); i++){
                    optio[i] = contextoListRetrofits.get(i).getTar_nombre();
                   // list.add(contextoListRetrofits.get(i).getTar_nombre());
                }


                //final CharSequence[] singleChoiceItems = list.toArray(new CharSequence[list.size()]);
                final String[] singleChoiceItems = optio;//getResources().getStringArray( R.array.arraycurrency );
                int itemSelected = 0;
                new AlertDialog.Builder( getApplicationContext() )
                        .setTitle( "Selecione el tipo de descuento" )
                        .setSingleChoiceItems( singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                                optiondescuento.setText( singleChoiceItems[selectedIndex] );
                            }
                        } )
                        .setPositiveButton( "Ok", null )
                        .setNegativeButton( "Cancel", null )
                        .show();

            }
        });
    }

    private void cargatContexto() {

        loadswet("Iniciado Sesión...");
        //registro(correo,password2);


        String empresaID = "";
        JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
        try {
            empresaID = roles.get("org_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<Object> obj = MyApiAdapter.getApiUtil().getContextoApp(empresaID);
        obj.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {


                Log.v("Resposnse", "" + response.body());

                String jsonData = response.body().toString();
                try {
                    JSONObject myJson = new JSONObject(jsonData);


                     contexto = new Contexto(myJson);
                     modelo.contexto = contexto;


                    contextoListRetrofits =  convert(contexto.getList_descuentos());

                   if(contexto.getList_descuentos().length() <=0){
                       option.setVisibility(View.GONE);
                   }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideDialog();

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.v("response t", t.getMessage());
                hideDialog();

            }
        });
    }


    public static ArrayList<ContextoListRetrofit> convert(JSONArray jArr)
    {
        ArrayList<ContextoListRetrofit> list = new ArrayList<ContextoListRetrofit>();
        try {
            for (int i=0, l=jArr.length(); i<l; i++){

                list.add(new ContextoListRetrofit((JSONObject) jArr.get(i)));
            }
        } catch (JSONException e) {}

        return list;
    }

    public static JSONArray convert(Collection<ContextoListRetrofit> list)
    {
        return new JSONArray(list);
    }




    //posgres dialos sweetalert

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    public void detalleclientefactura(View v) {

        if (modelo.formadepago.equals("")) {
            Toast.makeText(getApplicationContext(), "Seleccione la forma de pago", Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(getApplicationContext(), DatosClienteFactura.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            finish();
        }

    }


    public void tarjeta(View v) {
        modelo.formadepago = "tarjeta";
    }

    public void efectivo(View v) {
        modelo.formadepago = "efectivo";
    }
}