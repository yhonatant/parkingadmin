package com.parkinglot.parkingadmin.Retrofit.Configuracion;

import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by tacto on 8/08/17.
 */

public interface MyApiService {




    @FormUrlEncoded
    @POST("guardarApp")
    Call<Object> setVehiculo(
            @Field("empresaID") String empresaID,
            @Field("placa") String placa,
            @Field("fechaHora") String fechaHora,
            @Field("usuarioID") String usuarioID
    );

    @FormUrlEncoded
    @GET("listarApp")
    Call<ArrayList<VehiculoSalidaRetrofit>> getListarApp(
            @Field("empresaID") String empresaID
    );

    @FormUrlEncoded
    @POST("listarAppByFecha")
    Call<Object> getListarFechaApp(
            @Field("empresaID") String empresaID,
            @Field("fecha") String fecha
    );


    @FormUrlEncoded
    @POST("listarApp")
    Call<Object> getListarPorSalida(
            @Field("empresaID") String empresaID

    );

    @FormUrlEncoded
    @POST("getParqueoByID")
    Call<Object> _getgetParqueoByID(
            @Field("parqueoID") String empresaID

    );

    @FormUrlEncoded
    @POST("getParqueoByID")
    Call<Object> getParqueoByID(
            @Field("parqueoID") String empresaID

    );



}
