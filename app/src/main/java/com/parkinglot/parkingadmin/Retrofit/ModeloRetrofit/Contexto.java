package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Contexto {

    private String org_id;
    private String org_nombre;
    private String org_razon_social;
    private String org_logo;
    private String org_multa;
    private String org_tolerancia_salida;
    private JSONArray list_descuentos;
    private JSONArray list_pagos;

    public Contexto() {
    }

    public Contexto(JSONObject jsonObject) throws JSONException {

        this.org_id = jsonObject.getString("org_id");
        this.org_nombre = jsonObject.getString("org_nombre");
        this.org_razon_social = jsonObject.getString("org_razon_social");
        this.org_logo = jsonObject.getString("org_logo");
        this.org_multa = jsonObject.getString("org_multa");
        this.org_tolerancia_salida = jsonObject.getString("org_tolerancia_salida");

        if (jsonObject.getJSONArray("list_descuentos") != null || !jsonObject.getJSONArray("list_descuentos").equals("null")) {
            this.list_descuentos = jsonObject.getJSONArray("list_descuentos");

        } else {
            this.list_descuentos = new JSONArray();
        }

        if (jsonObject.getJSONArray("list_pagos") != null || !jsonObject.getJSONArray("list_pagos").equals("null")) {
            this.list_pagos = jsonObject.getJSONArray("list_pagos");

        } else {
            this.list_pagos = new JSONArray();
        }
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_nombre() {
        return org_nombre;
    }

    public void setOrg_nombre(String org_nombre) {
        this.org_nombre = org_nombre;
    }

    public String getOrg_razon_social() {
        return org_razon_social;
    }

    public void setOrg_razon_social(String org_razon_social) {
        this.org_razon_social = org_razon_social;
    }

    public String getOrg_logo() {
        return org_logo;
    }

    public void setOrg_logo(String org_logo) {
        this.org_logo = org_logo;
    }

    public String getOrg_multa() {
        return org_multa;
    }

    public void setOrg_multa(String org_multa) {
        this.org_multa = org_multa;
    }

    public String getOrg_tolerancia_salida() {
        return org_tolerancia_salida;
    }

    public void setOrg_tolerancia_salida(String org_tolerancia_salida) {
        this.org_tolerancia_salida = org_tolerancia_salida;
    }

    public JSONArray getList_descuentos() {
        return list_descuentos;
    }

    public void setList_descuentos(JSONArray list_descuentos) {
        this.list_descuentos = list_descuentos;
    }

    public JSONArray getList_pagos() {
        return list_pagos;
    }

    public void setList_pagos(JSONArray list_pagos) {
        this.list_pagos = list_pagos;
    }
}
