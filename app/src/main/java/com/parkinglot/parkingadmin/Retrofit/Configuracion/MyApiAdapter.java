package com.parkinglot.parkingadmin.Retrofit.Configuracion;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tacto on 8/08/17.
 */

public class MyApiAdapter {

    private static MyApiUtil API_UTIL;// la interface
    private static MyApiService API_SERVICE;// la interface

    public static MyApiUtil getApiUtil() { //utiliza patron singelton se utiliza una instancia

        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();//va a mostrar lo resultados de la peticion el codigo de respuetsa
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        String baseUrlUtil = "https://inbound1.simplex-erp.com/Utils/Parking/";

        //si api service es null se va instaciar de lo contrario va a devolver el objeto
        if (API_UTIL == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrlUtil)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()) // <-- usamos el log level
                    .build();
            API_UTIL = retrofit.create(MyApiUtil.class);
        }

        return API_UTIL;
    }


    public static MyApiService getApiService() { //utiliza patron singelton se utiliza una instancia

        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();//va a mostrar lo resultados de la peticion el codigo de respuetsa
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        String baseUrlService = "https://inbound1.simplex-erp.com/Parking/Parqueo/";

        //si api service es null se va instaciar de lo contrario va a devolver el objeto
        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrlService)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(MyApiService.class);
        }

        return API_SERVICE;
    }

}
