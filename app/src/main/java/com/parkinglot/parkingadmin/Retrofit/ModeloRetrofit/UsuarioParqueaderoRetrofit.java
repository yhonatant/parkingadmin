package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UsuarioParqueaderoRetrofit {


    private  String usu_codigo;
    private  String usu_email;
    private  String usu_activo;
    private  String usu_bloqueado;
    private  String usu_reiniciar;
    private  String usu_id;
    private  String usu_foto;
    private  String usu_sexo;
    private  String usu_nombre;
    private  String usu_area;
    private  String usu_telefono;
    private  String usu_usuario;
    private  String usu_tipo_doc;
    private  String usu_utc;
    private JSONObject roles;
    private JSONArray documentos;
    private  String info;



    public UsuarioParqueaderoRetrofit(){}

    public UsuarioParqueaderoRetrofit(JSONObject jsonObject) throws JSONException {
        this.usu_codigo = jsonObject.getString("usu_codigo");
        this.usu_email =  jsonObject.getString("usu_email");
        this.usu_activo =  jsonObject.getString("usu_activo");
        this.usu_bloqueado =  jsonObject.getString("usu_bloqueado");
        this.usu_reiniciar = jsonObject.getString("usu_reiniciar");
        this.usu_id =  jsonObject.getString("usu_id");
        this.usu_foto =  jsonObject.getString("usu_foto");
        this.usu_sexo =  jsonObject.getString("usu_sexo");
        this.usu_nombre = jsonObject.getString("usu_nombre");
        this.usu_area =  jsonObject.getString("usu_area");
        this.usu_telefono =  jsonObject.getString("usu_telefono");
        this.usu_usuario = jsonObject.getString("usu_usuario");
        this.usu_tipo_doc =  jsonObject.getString("usu_tipo_doc");
        this.usu_utc =  jsonObject.getString("usu_utc");
        this.roles = jsonObject.getJSONObject("roles");
        this.documentos = jsonObject.getJSONArray("documentos");

    }



    public String getUsu_codigo() {
        return usu_codigo;
    }

    public void setUsu_codigo(String usu_codigo) {
        this.usu_codigo = usu_codigo;
    }

    public String getUsu_email() {
        return usu_email;
    }

    public void setUsu_email(String usu_email) {
        this.usu_email = usu_email;
    }

    public String getUsu_activo() {
        return usu_activo;
    }

    public void setUsu_activo(String usu_activo) {
        this.usu_activo = usu_activo;
    }

    public String getUsu_bloqueado() {
        return usu_bloqueado;
    }

    public void setUsu_bloqueado(String usu_bloqueado) {
        this.usu_bloqueado = usu_bloqueado;
    }

    public String getUsu_reiniciar() {
        return usu_reiniciar;
    }

    public void setUsu_reiniciar(String usu_reiniciar) {
        this.usu_reiniciar = usu_reiniciar;
    }

    public String getUsu_id() {
        return usu_id;
    }

    public void setUsu_id(String usu_id) {
        this.usu_id = usu_id;
    }

    public String getUsu_foto() {
        return usu_foto;
    }

    public void setUsu_foto(String usu_foto) {
        this.usu_foto = usu_foto;
    }

    public String getUsu_sexo() {
        return usu_sexo;
    }

    public void setUsu_sexo(String usu_sexo) {
        this.usu_sexo = usu_sexo;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getUsu_area() {
        return usu_area;
    }

    public void setUsu_area(String usu_area) {
        this.usu_area = usu_area;
    }

    public String getUsu_telefono() {
        return usu_telefono;
    }

    public void setUsu_telefono(String usu_telefono) {
        this.usu_telefono = usu_telefono;
    }

    public String getUsu_usuario() {
        return usu_usuario;
    }

    public void setUsu_usuario(String usu_usuario) {
        this.usu_usuario = usu_usuario;
    }

    public String getUsu_tipo_doc() {
        return usu_tipo_doc;
    }

    public void setUsu_tipo_doc(String usu_tipo_doc) {
        this.usu_tipo_doc = usu_tipo_doc;
    }

    public String getUsu_utc() {
        return usu_utc;
    }

    public void setUsu_utc(String usu_utc) {
        this.usu_utc = usu_utc;
    }

    public JSONObject getRoles() {
        return roles;
    }

    public void setRoles(JSONObject roles) {
        this.roles = roles;
    }

    public JSONArray getDocumentos() {
        return documentos;
    }

    public void setDocumentos(JSONArray documentos) {
        this.documentos = documentos;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
