package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

public class VehiculoRetrofit {
    private  String empresaID;
    private  String placa;
    private  String fechaHora;
    private  String usuarioID;



    public VehiculoRetrofit(JSONObject jsonObject) throws JSONException {
        this.empresaID = jsonObject.getString("empresaID");
        this.placa =  jsonObject.getString("placa");
        this.fechaHora =  jsonObject.getString("fechaHora");
        this.usuarioID =  jsonObject.getString("usuarioID");

    }

    public String getEmpresaID() {
        return empresaID;
    }

    public void setEmpresaID(String empresaID) {
        this.empresaID = empresaID;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }
}
