package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

public class RutDni {

    private  String estado;
    private  String razon_social;
    private  String direccion;

    public RutDni(){}

    public RutDni(String estado, String razon_social, String direccion) {
        this.estado = estado;
        this.razon_social = razon_social;
        this.direccion = direccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
