package com.parkinglot.parkingadmin.Retrofit.viewRetrofit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.UsuarioParqueaderoRetrofit;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.view.Inicio;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleSalida extends Activity {

    Modelo modelo = Modelo.getInstance();
    SweetAlertDialog pDialog;
    String path;
    private static String APP_DIRECTORY = "parkingqr/";
    TextView idpar, placa, fecha, hora, tipo_tiempotranscurrido;

    LinearLayout layout_abonado;
    LinearLayout layout_pdf;
    Bitmap bitmap;

    String fname, fnamepdf;
    String timeStamp;
    QRGEncoder qrgEncoder;
    ImageView idIVQrcode, qr2;


    VehiculoSalidaRetrofit vehiculoSalidaRetrofit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detalle_salida);


        String parqueoID = modelo.vehiculoSalidaRetrofits.getPar_id();
        String salida = modelo._salisa;
        Log.v("", "");

        idpar = (TextView) findViewById(R.id.idpar);
        placa = (TextView) findViewById(R.id.placa);
        fecha = (TextView) findViewById(R.id.fecha);
        tipo_tiempotranscurrido = (TextView) findViewById(R.id.tipo_tiempotranscurrido);
        hora = (TextView) findViewById(R.id.hora);
        layout_abonado = (LinearLayout) findViewById(R.id.layout_abonado);
        layout_pdf = (LinearLayout) findViewById(R.id.layout_pdf);
        idIVQrcode = findViewById(R.id.idIVQrcode);
        qr2 = findViewById(R.id.qr2);

        try {
            loadswet("Iniciado Sesión...");
            //registro(correo,password2);


            Call<Object> obj = MyApiAdapter.getApiService()._getgetParqueoByID(parqueoID);
            obj.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {


                    Log.v("Resposnse", "" + response.body());

                    String jsonData = response.body().toString();
                    try {
                        JSONObject myJson = new JSONObject(jsonData);
                        System.out.print(myJson.get("usu_nombre"));

                        vehiculoSalidaRetrofit = new VehiculoSalidaRetrofit(myJson);

                        idpar.setText("#" + vehiculoSalidaRetrofit.getPar_id());
                        String[] arrSplit = vehiculoSalidaRetrofit.getPar_hora_ingreso().split(" ");
                        String option1 = arrSplit[0].trim();
                        String option2 = arrSplit[1].trim();


                        placa.setText(vehiculoSalidaRetrofit.getPar_placa());
                        fecha.setText(option1);
                        hora.setText(option2);

                        if (!vehiculoSalidaRetrofit.getPar_abonado().equals("")) {
                            layout_abonado.setVisibility(View.VISIBLE);
                        } else {
                            layout_abonado.setVisibility(View.GONE);
                        }

                        if (vehiculoSalidaRetrofit.getPar_hora_salida().equals("")) {


                            Date hora1 = new SimpleDateFormat().parse(vehiculoSalidaRetrofit.getPar_hora_ingreso());
                            Date hora2 = new SimpleDateFormat().parse(vehiculoSalidaRetrofit.getPar_hora_salida());
                            long lantes = hora1.getTime();
                            long lahora = hora2.getTime();
                            long diferencia = (lahora - lantes);
                            long minutes = (diferencia / 1000) / 60;
                            int Hours = (int) minutes / 60;
                            int Minutes = (int) minutes % 60;
                            tipo_tiempotranscurrido.setText(Hours + ":" + Minutes);
                            Date tiempo = modelo.getDifferenceBetwenDates(hora1, hora2);

                            tipo_tiempotranscurrido.setText("" + tiempo);

                        } else {
                            tipo_tiempotranscurrido.setText("" + modelo._salisa);
                        }

                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }

                    generarQR();
                    hideDialog();

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.v("response t", t.getMessage());
                    hideDialog();
                    // alerta("Guardar Datos","Desea recordar los datos de ingreso para este dispositivo.");
                }
            });


        } catch (Exception e) {
        }
    }


    public void detalleclientefactura(View v) {

        /*if (modelo.formadepago.equals("")) {
            Toast.makeText(getApplicationContext(), "Seleccione la forma de pago", Toast.LENGTH_LONG).show();
        } else {*/
            Intent i = new Intent(getApplicationContext(), DatosClienteFactura.class);
            startActivity(i);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            finish();
        //}

    }

    //posgres dialos sweetalert

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    public void genearqr(View v) {
        geneaarqr();
    }

    private void geneaarqr() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void geneaarqr(View v) {

        Log.d("size", "msm" + layout_pdf.getWidth() + " " + layout_pdf.getHeight());

        bitmap = LoadBitmap(layout_pdf, layout_pdf.getWidth(), layout_pdf.getHeight());

        CreatePdf();

    }


    //pdf

    private Bitmap LoadBitmap(View v, int width, int height) {

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        v.draw(canvas);
        return bitmap;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void CreatePdf() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float whitch = displayMetrics.widthPixels;
        float height = displayMetrics.heightPixels;
        int convertWitch = (int) whitch, convertHeight = (int) height - 300;
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWitch, convertHeight, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        canvas.drawPaint(paint);
        bitmap = Bitmap.createScaledBitmap(bitmap, convertWitch, convertHeight, true);
        canvas.drawBitmap(bitmap, 0, 0, null);
        pdfDocument.finishPage(page);
        //target pdf dowload
        path = "/storage/emulated/0/parkingqr";
        //String targetPdf = "/storage/emulated/0/parkingqr/page.pdf";
        //File file;
        //file = new File(path);
        try {


            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            path = "/storage/emulated/0/parkingqr/";
            fname = "qr_" + timeStamp + ".jpg";


            //String root = Environment.getExternalStorageDirectory().toString();
            File root = android.os.Environment.getExternalStorageDirectory();
            File myDir = new File(path);
            fnamepdf = timeStamp + ".pdf";

            // File myDir = new File(dir + "/saved_images");
            myDir.mkdirs();


            File file = new File(myDir, fnamepdf);
            if (file.exists()) file.delete();

            pdfDocument.writeTo(new FileOutputStream(file));// FileOutputStream out = new FileOutputStream(file);
            // out.flush();
            //out.close();

            try {

                try {
                    Thread.sleep(2000);
                    openPdf();
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

            } catch (Exception e) {
                Log.v("error", e.getMessage());
            }
            //pdfDocument.writeTo(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "algo mal intenta de nuevo", Toast.LENGTH_LONG).show();
            //después del cierre del documento
            pdfDocument.close();
            Toast.makeText(getApplicationContext(), "Archivo descargado", Toast.LENGTH_LONG).show();

            try {
                Thread.sleep(2000);
                openPdf();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }


        }
    }


    ///PDF


    private void openPdf() {

        File myDir = new File(path);
        fnamepdf = timeStamp + ".pdf";
        //    File path = new File(getActivity().getFilesDir(), "parqueadero");

        File file = new File(myDir, fnamepdf);
        if (file.exists()) {

            Uri uri;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= 24) {
                uri = FileProvider.getUriForFile(getApplicationContext(),
                        "com.parkinglot.parkingadmin", file);

                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            } else {
                uri = Uri.fromFile(file);
                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            }
            // Así va correctamente la dirección
            String dir = Environment.getExternalStorageDirectory() + APP_DIRECTORY + fname;

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                //if user doesn't have pdf reader instructing to download a pdf reader
            }


        } else {
            Toast.makeText(getApplicationContext(), "El archivo no existe", Toast.LENGTH_LONG).show();

        }

    }

    public void generarQR() {


        //below line is for getting the windowmanager service.
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        //initializing a variable for default display.
        Display display = manager.getDefaultDisplay();
        //creating a variable for point which is to be displayed in QR Code.
        Point point = new Point();
        display.getSize(point);
        //getting width and height of a point
        int width = point.x;
        int height = point.y;
        //generating dimension from width and height.
        int dimen = width < height ? width : height;
        dimen = dimen * 3 / 4;

        String empresaID = "";
        JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
        try {
            empresaID = roles.get("org_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String finalEmpresaID = empresaID;


        //setting getActivity() dimensions inside our qr code encoder to generate our qr code.
        String dataQR = vehiculoSalidaRetrofit.getPar_id() + "_" + vehiculoSalidaRetrofit.getPar_hora_ingreso() + "_" + vehiculoSalidaRetrofit.getPar_placa() + "_" + empresaID;
        qrgEncoder = new QRGEncoder(dataQR, null, QRGContents.Type.TEXT, dimen);
        try {
            //getting our qrcode in the form of bitmap.
            bitmap = qrgEncoder.encodeAsBitmap();
            // the bitmap is set inside our image view using .setimagebitmap method.


            try {

                idIVQrcode.setImageBitmap(bitmap);
                qr2.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (WriterException e) {
            //getActivity() method is called for exception handling.
            Log.e("Tag", e.toString());
        }

    }


    public void tarjeta(View v) {
        modelo.formadepago = "tarjeta";
    }

    public void efectivo(View v) {
        modelo.formadepago = "efectivo";
    }
}