package com.parkinglot.parkingadmin.Retrofit.viewRetrofit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.RutDni;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.UsuarioParqueaderoRetrofit;
import com.parkinglot.parkingadmin.model.Modelo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidmads.library.qrgenearator.QRGEncoder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DatosClienteFactura extends Activity {

    LinearLayout layout_pdf;
    Bitmap bitmap;
    String fname, fnamepdf;
    String timeStamp;
    Modelo modelo = Modelo.getInstance();
    SweetAlertDialog pDialog;
    String path;
    private static String APP_DIRECTORY = "parkingqr/";

    ImageView searchlupa;

    private EditText nitrut, razonsocial, numoperacion, direccion;
    private CheckBox checkBox2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_datos_cliente_factura);

        nitrut = (EditText) findViewById(R.id.nitrut);
        razonsocial = (EditText) findViewById(R.id.razonsocial);
        numoperacion = (EditText) findViewById(R.id.numoperacion);
        direccion = (EditText) findViewById(R.id.direccion);
        checkBox2 = (CheckBox) findViewById(R.id.checkbox_cheese);
        searchlupa = (ImageView) findViewById(R.id.searchlupa);


        if (modelo.formadepago.equals("efectivo")) {
            numoperacion.setClickable(false);
            numoperacion.setFocusable(false);
            numoperacion.setBackgroundResource(R.color.negro_semitrasnparente);
        }


        checkBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {

                    if (modelo.formadepago.equals("efectivo") || modelo.formadepago.equals("tarjeta")) {
                        nitrut.setClickable(false);
                        nitrut.setFocusable(false);
                        nitrut.setBackgroundResource(R.color.negro_semitrasnparente);
                        searchlupa.setClickable(false);
                        searchlupa.setBackgroundResource(R.color.negro_semitrasnparente);

                    }
                } else {
                    nitrut.setClickable(true);
                    nitrut.setFocusable(true);
                    nitrut.setBackgroundResource(R.color.mdtp_white);
                    searchlupa.setClickable(true);
                    searchlupa.setBackgroundResource(R.color.colorApp);

                }
            }
        });
    }


    //posgres dialos sweetalert

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    public void genearqr(View v) {
        geneaarqr();
    }

    private void geneaarqr() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void geneaarqr(View v) {

        Log.d("size", "msm" + layout_pdf.getWidth() + " " + layout_pdf.getHeight());

        bitmap = LoadBitmap(layout_pdf, layout_pdf.getWidth(), layout_pdf.getHeight());

        CreatePdf();

    }


    //pdf

    private Bitmap LoadBitmap(View v, int width, int height) {

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        v.draw(canvas);
        return bitmap;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void CreatePdf() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float whitch = displayMetrics.widthPixels;
        float height = displayMetrics.heightPixels;
        int convertWitch = (int) whitch, convertHeight = (int) height - 300;
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWitch, convertHeight, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        canvas.drawPaint(paint);
        bitmap = Bitmap.createScaledBitmap(bitmap, convertWitch, convertHeight, true);
        canvas.drawBitmap(bitmap, 0, 0, null);
        pdfDocument.finishPage(page);
        //target pdf dowload
        path = "/storage/emulated/0/parkingqr";
        //String targetPdf = "/storage/emulated/0/parkingqr/page.pdf";
        //File file;
        //file = new File(path);
        try {


            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            path = "/storage/emulated/0/parkingqr/";
            fname = "qr_" + timeStamp + ".jpg";


            //String root = Environment.getExternalStorageDirectory().toString();
            File root = android.os.Environment.getExternalStorageDirectory();
            File myDir = new File(path);
            fnamepdf = timeStamp + ".pdf";

            // File myDir = new File(dir + "/saved_images");
            myDir.mkdirs();


            File file = new File(myDir, fnamepdf);
            if (file.exists()) file.delete();

            pdfDocument.writeTo(new FileOutputStream(file));// FileOutputStream out = new FileOutputStream(file);
            // out.flush();
            //out.close();

            try {

                try {
                    Thread.sleep(2000);
                    openPdf();
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

            } catch (Exception e) {
                Log.v("error", e.getMessage());
            }
            //pdfDocument.writeTo(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "algo mal intenta de nuevo", Toast.LENGTH_LONG).show();
            //después del cierre del documento
            pdfDocument.close();
            Toast.makeText(getApplicationContext(), "Archivo descargado", Toast.LENGTH_LONG).show();

            try {
                Thread.sleep(2000);
                openPdf();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }


        }
    }


    ///PDF


    private void openPdf() {

        File myDir = new File(path);
        fnamepdf = timeStamp + ".pdf";
        //    File path = new File(getActivity().getFilesDir(), "parqueadero");

        File file = new File(myDir, fnamepdf);
        if (file.exists()) {

            Uri uri;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= 24) {
                uri = FileProvider.getUriForFile(getApplicationContext(),
                        "com.parkinglot.parkingadmin", file);

                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            } else {
                uri = Uri.fromFile(file);
                intent.setDataAndType(uri, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            }
            // Así va correctamente la dirección
            String dir = Environment.getExternalStorageDirectory() + APP_DIRECTORY + fname;

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                //if user doesn't have pdf reader instructing to download a pdf reader
            }


        } else {
            Toast.makeText(getApplicationContext(), "El archivo no existe", Toast.LENGTH_LONG).show();

        }

    }


    public void search(View v) {
        String dnirut = nitrut.getText().toString();
        if (dnirut.equals("")) {
            Toast.makeText(getApplicationContext(), "Ingrese el Dni o Rut", Toast.LENGTH_LONG).show();
        } else {



            Call<RutDni> obj = null;
            if(dnirut.length() == 8){
               obj = MyApiAdapter.getApiUtil().getDniExt(dnirut);
            }else  if(dnirut.length() == 10 || dnirut.length() == 11){
                obj = MyApiAdapter.getApiUtil().getRucExt(dnirut);
            }else{
                Toast.makeText(getApplicationContext(), "Dni o Rut erróneo", Toast.LENGTH_LONG).show();
                return;
            }

            loadswet("Cargando información...");
            //registro(correo,password2);
            obj.enqueue(new Callback<RutDni>() {
                @Override
                public void onResponse(Call<RutDni> call, Response<RutDni> response) {


                    Log.v("Resposnse", "" + response.body());

                       try {

                           RutDni rutDni = response.body();
                           razonsocial.setText(rutDni.getRazon_social()+"");
                           direccion.setText(rutDni.getDireccion()+"");





                       } catch (Exception e) {
                        e.printStackTrace();
                    }

                    hideDialog();

                }

                @Override
                public void onFailure(Call<RutDni> call, Throwable t) {
                    Log.v("response t", t.getMessage());
                    hideDialog();
                    // alerta("Guardar Datos","Desea recordar los datos de ingreso para este dispositivo.");
                }
            });

        }
    }


    public String convertStandardJSONString(String data_json){
        data_json = data_json.replace("\\", "");
        data_json = data_json.replace("\"{", "{");
        data_json = data_json.replace("}\",", "},");
        data_json = data_json.replace("}\"", "}");
        return data_json;
    }

    public void voleta(View v) {
        modelo.voletafactura = "volta";
    }

    public void factura(View v) {
        modelo.voletafactura = "factura";
    }
}