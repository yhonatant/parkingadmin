package com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

public class VehiculoSalidaRetrofit {


    private String par_id;
    private String loc_nombre;
    private String par_placa;
    private String par_hora_ingreso;
    private String par_hora_salida;
    private String par_estado;
    private String loc_padre;
    private String par_abonado;
    private String par_min_pagados;
    private String par_min_libres;
    private String usu_nombre;
    private String par_min_salir;
    private String imagen;

    public VehiculoSalidaRetrofit() {

    }

    public VehiculoSalidaRetrofit(JSONObject jsonObject) throws JSONException {

        this.par_id = jsonObject.getString("par_id");
        this.loc_nombre = jsonObject.getString("loc_nombre");
        this.par_placa = jsonObject.getString("par_placa");
        this.par_hora_ingreso = jsonObject.getString("par_hora_ingreso");

        if (jsonObject.getString("par_hora_salida") == null || jsonObject.getString("par_hora_salida").equals("null")) {
            this.par_hora_salida = "";
        } else {
            this.par_hora_salida = jsonObject.getString("par_hora_salida");
        }


        this.par_estado = jsonObject.getString("par_estado");
        this.loc_padre = jsonObject.getString("loc_padre");

        if (jsonObject.getString("par_abonado") == null || jsonObject.getString("par_abonado").equals("null")) {
            this.par_abonado = "";
        } else {
            this.par_abonado = jsonObject.getString("par_abonado");
        }

        if (jsonObject.getString("par_min_pagados") == null || jsonObject.getString("par_min_pagados").equals("null")) {
            this.par_min_pagados = "";
        } else {
            this.par_min_pagados = jsonObject.getString("par_min_pagados");
        }


        if (jsonObject.getString("par_min_libres") == null || jsonObject.getString("par_min_libres").equals("null")) {
            this.par_min_libres = "";
        } else {
            this.par_min_libres = jsonObject.getString("par_min_libres");

        }
        this.usu_nombre = jsonObject.getString("usu_nombre");

        if (jsonObject.getString("par_min_salir") == null  || jsonObject.getString("par_min_salir").equals("null")) {
            this.par_min_salir = "";
        } else {
            this.par_min_salir = jsonObject.getString("par_min_salir");

        }


    }

    public String getPar_id() {
        return par_id;
    }

    public void setPar_id(String par_id) {
        this.par_id = par_id;
    }

    public String getLoc_nombre() {
        return loc_nombre;
    }

    public void setLoc_nombre(String loc_nombre) {
        this.loc_nombre = loc_nombre;
    }

    public String getPar_placa() {
        return par_placa;
    }

    public void setPar_placa(String par_placa) {
        this.par_placa = par_placa;
    }

    public String getPar_hora_ingreso() {
        return par_hora_ingreso;
    }

    public void setPar_hora_ingreso(String par_hora_ingreso) {
        this.par_hora_ingreso = par_hora_ingreso;
    }

    public String getPar_hora_salida() {
        return par_hora_salida;
    }

    public void setPar_hora_salida(String par_hora_salida) {
        this.par_hora_salida = par_hora_salida;
    }

    public String getPar_estado() {
        return par_estado;
    }

    public void setPar_estado(String par_estado) {
        this.par_estado = par_estado;
    }

    public String getLoc_padre() {
        return loc_padre;
    }

    public void setLoc_padre(String loc_padre) {
        this.loc_padre = loc_padre;
    }

    public String getPar_abonado() {
        return par_abonado;
    }

    public void setPar_abonado(String par_abonado) {
        this.par_abonado = par_abonado;
    }

    public String getPar_min_pagados() {
        return par_min_pagados;
    }

    public void setPar_min_pagados(String par_min_pagados) {
        this.par_min_pagados = par_min_pagados;
    }

    public String getPar_min_libres() {
        return par_min_libres;
    }

    public void setPar_min_libres(String par_min_libres) {
        this.par_min_libres = par_min_libres;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getPar_min_salir() {
        return par_min_salir;
    }

    public void setPar_min_salir(String par_min_salir) {
        this.par_min_salir = par_min_salir;
    }
}
