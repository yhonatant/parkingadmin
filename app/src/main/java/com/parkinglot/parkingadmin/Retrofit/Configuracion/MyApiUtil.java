package com.parkinglot.parkingadmin.Retrofit.Configuracion;

import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.RutDni;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by tacto on 8/08/17.
 */

public interface MyApiUtil {

    //ws login

    @FormUrlEncoded
    @POST("getLoginApp")
    Call<Object> getLogin(
            @Field("usuario") String correo,
            @Field("clave") String password
    );

    @FormUrlEncoded
    @POST("getRucExt")
    Call<RutDni> getRucExt(
            @Field("valor") String valor
    );


    @FormUrlEncoded
    @POST("getDniExt")
    Call<RutDni> getDniExt(
            @Field("valor") String valor
    );


    @FormUrlEncoded
    @POST("getContextoApp")
    Call<Object> getContextoApp(
            @Field("empresaID") String empresaID
    );

    @FormUrlEncoded
    @POST("getQR2")
    Call<Object> getQR2(
            @Field("valor") String valor
    );




}
