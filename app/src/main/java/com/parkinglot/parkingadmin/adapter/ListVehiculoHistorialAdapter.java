package com.parkinglot.parkingadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.SupportMapFragment;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.fragment.CajaFragment;
import com.parkinglot.parkingadmin.fragment.ListVehiculoFragment;
import com.parkinglot.parkingadmin.fragment.Salida;
import com.parkinglot.parkingadmin.model.Clases.HistoricoVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.view.SalidaVehiculo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ListVehiculoHistorialAdapter extends RecyclerView.Adapter<ListVehiculoHistorialAdapter.HistoricoVehiculoParqueadoViewHolder> implements Filterable {

    private Context context;
    private List<HistoricoVehiculoParqueado> listVehiculos;
    private List<HistoricoVehiculoParqueado> listaHistoricoVehiculoParqueado;
    Modelo modelo = Modelo.getInstance();


    public ListVehiculoHistorialAdapter(Context context, List<HistoricoVehiculoParqueado> listVehiculos) {
        super();
        this.context = context;
        this.listVehiculos = listVehiculos;
        this.listaHistoricoVehiculoParqueado = listVehiculos;


    }

    @NonNull
    @Override
    public HistoricoVehiculoParqueadoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate( R.layout.adapter_vehiculo_historial, viewGroup, false);
        return new HistoricoVehiculoParqueadoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricoVehiculoParqueadoViewHolder holder, final int position) {
        String nombreParwueadero = modelo.parqueadero.getNombreParqueadero();
        String fecha = listaHistoricoVehiculoParqueado.get(position).getFecha();
        String fechaFin = listaHistoricoVehiculoParqueado.get(position).getFechaFin();
        String placa = listaHistoricoVehiculoParqueado.get(position).getPlaca();
        String estado = listaHistoricoVehiculoParqueado.get(position).getEstado();
        String tiempo =  modelo.parqueadero.getformaDeCobro();
        String moneda =  modelo.parqueadero.getTipoMoneda();
        double precio = modelo.parqueadero.getPrecio();
        int duracion =  listaHistoricoVehiculoParqueado.get(position).getTiempoDeParqueo();
        double cancelo =  listaHistoricoVehiculoParqueado.get(position).getCobro();


        String[] arrSplit = fecha.split(" ");
        String option1 =arrSplit[0].trim();
        String option2 =arrSplit[1].trim();
        String option3 =arrSplit[2].trim();
        String option4 =arrSplit[3].trim();

        String[] arrSplit2 = fechaFin.split(" ");
        String option5 =arrSplit2[0].trim();
        String option6 =arrSplit2[1].trim();
        String option7 =arrSplit2[2].trim();
        String option8 =arrSplit2[3].trim();


        holder.txtparqueadero.setText(nombreParwueadero );
        holder.txtfecha.setText(option1);
        holder.txtfecha.setText(option1);
        holder.txthora1.setText(option2+" "+option3+option4);
        holder.txthora2.setText(option6+" "+option7+option8);
        holder.txtplaca.setText(placa);
        holder.txtestado.setText("Estado: " +estado);
        holder.tiempo.setText(tiempo );
        holder.valorcobro.setText( "Fracción: "+precio +" "+ moneda  );
        holder.duracion.setText("Duración: "+  duracion+" Minutos"  );
        holder.cancelo.setText(  "Cancelo: "+ cancelo+" "+ moneda  );












    }


    @Override
    public int getItemCount() {
        return listaHistoricoVehiculoParqueado.size();
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * <p>This method is usually implemented by {@link RecyclerView.Adapter}
     * classes.</p>
     *
     * @return a filter used to constrain data
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    listaHistoricoVehiculoParqueado = listVehiculos;
                } else {
                    List<HistoricoVehiculoParqueado> filteredList = new ArrayList<>();
                    for (HistoricoVehiculoParqueado name : listVehiculos) {
                        if (name.getPlaca().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        listaHistoricoVehiculoParqueado = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = listaHistoricoVehiculoParqueado;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaHistoricoVehiculoParqueado = (List<HistoricoVehiculoParqueado>) results.values;
                notifyDataSetChanged();
            }
        };
    }




    class HistoricoVehiculoParqueadoViewHolder extends RecyclerView.ViewHolder {

        private TextView txtfecha;
        private TextView txtparqueadero;
        private TextView txtplaca;
        private TextView txtestado;
        private TextView valorcobro;
        private TextView tiempo;
        private TextView duracion;
        private TextView cancelo;

        private TextView txthora1;
        private TextView txthora2;
        private Button btnfinalizar;


        HistoricoVehiculoParqueadoViewHolder(@NonNull View itemView) {
            super(itemView);
            txtfecha = itemView.findViewById(R.id.txtfecha);
            txtparqueadero = itemView.findViewById(R.id.txtparqueadero);
            txtplaca = itemView.findViewById(R.id.txtplaca);
            txtestado = itemView.findViewById(R.id.txtestado);
            valorcobro = itemView.findViewById(R.id.valorcobro);
            tiempo = itemView.findViewById(R.id.tiempo);
            duracion = itemView.findViewById(R.id.duracion);
            cancelo = itemView.findViewById(R.id.cancelo);
            txthora1 = itemView.findViewById(R.id.txthora1);
            txthora2 = itemView.findViewById(R.id.txthora2);
            btnfinalizar = itemView.findViewById(R.id.btnfinalizar);

        }
    }




}