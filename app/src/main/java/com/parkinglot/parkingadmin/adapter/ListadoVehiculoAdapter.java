package com.parkinglot.parkingadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.Retrofit.viewRetrofit.DetalleFacturaRetrofit;
import com.parkinglot.parkingadmin.Retrofit.viewRetrofit.DetalleSalida;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;
import com.parkinglot.parkingadmin.view.Login;
import com.parkinglot.parkingadmin.view.SalidaVehiculo;

import org.joda.time.Instant;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ListadoVehiculoAdapter extends RecyclerView.Adapter<ListadoVehiculoAdapter.VehiculoSalidaRetrofitViewHolder>{

    private Context context;
    private List<VehiculoSalidaRetrofit> listVehiculos;
    private List<VehiculoSalidaRetrofit> listaVehiculoSalidaRetrofit;
    Modelo modelo = Modelo.getInstance();
  
    Utility utility;

    String option3 ="";
    String option4 ="";

    String txtsalida ="";

    Date hora1,hora2,hora3;
    public ListadoVehiculoAdapter(Context context, List<VehiculoSalidaRetrofit> listVehiculos) {
        super();
        this.context = context;
        this.listVehiculos = listVehiculos;
        this.listaVehiculoSalidaRetrofit = listVehiculos;
        utility = new Utility();

    }

    @NonNull
    @Override
    public ListadoVehiculoAdapter.VehiculoSalidaRetrofitViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_vehiculo_parqueadero, viewGroup, false);
        return new ListadoVehiculoAdapter.VehiculoSalidaRetrofitViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ListadoVehiculoAdapter.VehiculoSalidaRetrofitViewHolder holder, final int position) {
        String _key = listaVehiculoSalidaRetrofit.get(position).getPar_id();
        String _placa = listaVehiculoSalidaRetrofit.get(position).getPar_placa();
        String _ingreso = listaVehiculoSalidaRetrofit.get(position).getPar_hora_ingreso();

        String _salisa = listaVehiculoSalidaRetrofit.get(position).getPar_hora_salida();
        String _tiempoExtra ="";

        if(_salisa.equals("")){
            String _abonado = listaVehiculoSalidaRetrofit.get(position).getPar_abonado();
            txtsalida = "";
            if(_abonado.equals("")){
                //calculo horasalida  y hora entrada
                try {

                    //fecha y hora actual
                    Date date = new Date();
                    String fechacComplString = modelo._formatter.format(date);

                     hora1 = modelo.ParseFecha(_ingreso);
                     hora2 = modelo.ParseFecha(fechacComplString);



                     int hor2 =hora2.getHours();
                    int hor1 =hora1.getHours();

                    if(hor1 > hor2){
                        int _hor2 = hor1;
                        int _hor1 = hor2;

                        hor2 = _hor2;
                        hor1 = _hor1;

                    }
                     int horas  = (hor2 - hor1);
                    int dias  = (hora2.getDay() - hora1.getDay());
                    int min  = (hora2.getMinutes()- hora1.getMinutes());


                    int  tiempoHoras = horas +(dias*24);
                    int horasmint = 0;
                    if(tiempoHoras> 0){
                        if(min > 10){
                            horasmint= 1;
                        }
                        _salisa =(tiempoHoras+horasmint)+"";
                    }else{
                        _salisa =min+"";
                    }

                   /* Date res = modelo.getDifferenceBetwenDates(hora2,hora1);



                    String _fechacComplString = modelo._formatter.format(res);
                    Date res2 = modelo.ParseFecha(_fechacComplString );


                    String fechacComplString3 = modelo._formatter.format(res2);

                    String[] arrSplit3 =fechacComplString3.split(" ");
                    String option5 =arrSplit3[0].trim();
                    String option6 =arrSplit3[1].trim();


                    int mills1 = res.getHours();*/



                }catch (Exception e){}

            }else{
                holder.estrella.setVisibility(View.VISIBLE);
                _salisa = listaVehiculoSalidaRetrofit.get(position).getPar_abonado();
            }

        }else{
            //hacer calculo hora salida y hora actual
          //  _tiempoExtra = "18 min";
            txtsalida =_salisa;
            holder.tiepoestra.setVisibility(View.VISIBLE);


            hora3= modelo.ParseFecha(_salisa);

            String[] arrSplit1 = _salisa.split(" ");
            option3 =arrSplit1[0].trim();
            option4 =arrSplit1[1].trim();
            _salisa =option4;




           long diferencia=hora1.getTime()-hora3.getTime();
          long minutos = TimeUnit.MILLISECONDS.toMinutes(diferencia);




            String org_tolerancia ="";
            JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
            try {
                org_tolerancia = roles.get("org_tolerancia").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int  _org_tolerancia = Integer.parseInt(org_tolerancia) ;


          if(_org_tolerancia  > minutos){
              holder.tiepoestra.setTextColor(context.getResources().getColor(R.color.negro_semitrasnparente2));
          }else{
              holder.tiepoestra.setTextColor(context.getResources().getColor(R.color.colorApp));
          }
            _tiempoExtra = "+"+ minutos+" min";
        }


        //fecha
        String[] arrSplit = _ingreso.split(" ");
        String option1 =arrSplit[0].trim();
        String option2 =arrSplit[1].trim();


        holder.txtparqueadero.setText("");
        holder.txtfecha.setText(_ingreso);
        holder.txtplaca.setText(_placa);
        holder.tiempo.setText(_ingreso);
        holder.txthora1.setText(""+option2);
        holder.txthora2.setText(_salisa);
        holder.tiepoestra.setText(_tiempoExtra);
        holder.codigo.setText(_key);




        String salida ="";
        JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
        try {
            salida = roles.get("es_salida").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String _salidau = salida;

        if(_salidau.equals("Y")){

            if(txtsalida.equals("")){
                holder.btnfinalizar.setBackgroundResource(R.drawable.round_bg_gris);
            }else {
                holder.btnfinalizar.setBackgroundResource(R.drawable.round_bg);
                holder.btnfinalizar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(context, "Acción terminar", Toast.LENGTH_LONG).show();

                        String data =  modelo.optionTapas;
                        modelo.vehiculoSalidaRetrofits =  listaVehiculoSalidaRetrofit.get(position);

                          modelo._salisa = holder.txthora2.getText().toString();

                        if(data.equals("salida") ||  data.equals("inicio")){
                            Intent i = new Intent(context,DetalleSalida.class);
                            context.startActivity(i);

                        }else{
                            Intent i = new Intent(context, DetalleFacturaRetrofit.class);
                            context.startActivity(i);
                        }


                    }
                });
            }

        }
        else {

            holder.btnfinalizar.setBackgroundResource(R.drawable.round_bg_gris);
        }


    }


    @Override
    public int getItemCount() {
        return listaVehiculoSalidaRetrofit.size();
    }


    class VehiculoSalidaRetrofitViewHolder extends RecyclerView.ViewHolder {

        private TextView txtfecha;
        private TextView txtparqueadero;
        private TextView txtplaca;
        private TextView tiempo;
        private TextView txthora1;
        private TextView txthora2;
        private ImageView estrella;
        private TextView tiepoestra;
        private  Button btnfinalizar;
        private  TextView codigo;


        VehiculoSalidaRetrofitViewHolder(@NonNull View itemView) {
            super(itemView);
            txtfecha = itemView.findViewById(R.id.txtfecha);
            txtparqueadero = itemView.findViewById(R.id.txtparqueadero);
            txtplaca = itemView.findViewById(R.id.txtplaca);

            tiempo = itemView.findViewById(R.id.tiempo);
            txthora1 = itemView.findViewById(R.id.txthora1);
            txthora2 = itemView.findViewById(R.id.txthora2);
            estrella = itemView.findViewById(R.id.estrella);
            tiepoestra  = itemView.findViewById(R.id.tiepoestra);
            btnfinalizar = itemView.findViewById(R.id.btnfinalizar);
            codigo = itemView.findViewById(R.id.codigo);

        }
    }





}
