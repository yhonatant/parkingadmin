package com.parkinglot.parkingadmin.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.ui.historial.HistorialFragment;
import com.parkinglot.parkingadmin.view.Login;
import com.parkinglot.parkingadmin.view.SalidaVehiculo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeerQRFragment extends Fragment{


    Modelo modelo = Modelo.getInstance();
    private CameraSource cameraSource;
    private SurfaceView cameraView;
    private final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private String token = "";
    private String tokenanterior = "";
    String dataQr="";
    LinearLayout layoutqr;
    LinearLayout layoutoption;
    LinearLayout layoutcamara;
    LinearLayout layoutmanual;
    Space paceid;

    TextView txtqr;
    SweetAlertDialog pDialog;
    public LeerQRFragment() {
        // Required empty public constructor
    }

    public static LeerQRFragment newInstance(String param1, String param2) {
        LeerQRFragment fragment = new LeerQRFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_qr, container, false);

        cameraView = (SurfaceView) view.findViewById(R.id.camera_view);
        txtqr = (TextView) view.findViewById(R.id.txtqr);
        txtqr = (TextView) view.findViewById(R.id.txtqr);
        layoutqr = (LinearLayout) view.findViewById(R.id.layoutqr);
        layoutoption = (LinearLayout) view.findViewById(R.id.layoutoption);
        layoutcamara = (LinearLayout) view.findViewById(R.id.layoutcamara);
        layoutmanual = (LinearLayout) view.findViewById(R.id.layoutmanual);
        paceid = (Space) view.findViewById(R.id.paceid);
        initQR();


        layoutcamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutqr.setVisibility(View.VISIBLE);
                layoutcamara.setVisibility(View.GONE);
                paceid.setVisibility(View.GONE);

            }
        });

        layoutoption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                String fecha = modelo.formatUniversal.format(date);


                String empresaID ="";
                JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
                try {
                    empresaID = roles.get("org_id").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String finalEmpresaID = empresaID;

                loadswet("Cargando Vehiculos...");
                //registro(correo,password2);

                try {


                    Call<Object> obj = MyApiAdapter.getApiService().getListarFechaApp(finalEmpresaID,"2021-06-16");
                    obj.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {


                            Log.v("Resposnse", "" + response.body());

                            String jsonData = response.body().toString();




                            try {
                                JSONArray myJson = new JSONArray(jsonData);
                                modelo.listVehiculoSalidaRetrofits.clear();

                                if(myJson != null){

                                    for (int i =0; i < myJson.length(); i++){

                                        Log.v("",""+myJson.get(i) );
                                        modelo.listVehiculoSalidaRetrofits.add(new VehiculoSalidaRetrofit((JSONObject) myJson.get(i)));
                                    }

                                }

                                int cant=   modelo.listVehiculoSalidaRetrofits.size();

                                Fragment fragment  = new ListadoVehiculos();
                                loadFragment(fragment);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            hideDialog();

                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Log.v("response t", t.getMessage());
                            hideDialog();
                            // alerta("Guardar Datos","Desea recordar los datos de ingreso para este dispositivo.");
                        }
                    });
                }catch (Exception e){
                    String error =  e.getMessage();
                    Log.v("Error", e.getLocalizedMessage());
                }
               //Fragment fragment  = new HistorialFragment();
                //loadFragment(fragment);
            }
        });
        return view;
    }


    public void initQR() {

        // creo el detector qr
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getActivity())
                        .setBarcodeFormats( Barcode.ALL_FORMATS)
                        .build();

        // creo la camara
        cameraSource = new CameraSource
                .Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        // listener de ciclo de vida de la camara
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                // verifico si el usuario dio los permisos para la camara
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // verificamos la version de ANdroid que sea al menos la M para mostrar
                        // el dialog de la solicitud de la camara
                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.CAMERA)) ;
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                    return;
                } else {
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException ie) {
                        Log.e("CAMERA SOURCE", ie.getMessage());
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        // preparo el detector de QR
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }


            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() > 0) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try{
                                token = barcodes.valueAt(0).displayValue.toString();


                                String  dataqr = token;

                                txtqr.setText( dataqr );
                                String[] parts = dataqr.split("\\|");
                                String part1 = parts[0]; // 123
                                String part2 = parts[1];

                                VehiculoParqueado vehiculoParqueado = new VehiculoParqueado(  );
                                for (int i = 0; i <modelo.listVehiculoParqueado.size(); i++){
                                    if (modelo.listVehiculoParqueado.get( i ).getPlaca().equals( part1 )){
                                      modelo.vehiculoParqueado =  modelo.listVehiculoParqueado.get( i );
                                        break;
                                    }
                                }

                                if(modelo.vehiculoParqueado != null){

                                    if(modelo.vehiculoParqueado.getKey() != null){

                                        cameraSource.stop();

                                        Intent i = new Intent(getActivity(), SalidaVehiculo.class);
                                        i.putExtra("key",modelo.vehiculoParqueado.getKey());
                                        startActivity(i);

                                    }
                                    else{
                                        Toast.makeText( getActivity(), "Placa no considen", Toast.LENGTH_LONG ).show();
                                    }
                                }else{
                                    Toast.makeText( getActivity(), "Placa no considen", Toast.LENGTH_LONG ).show();
                                }

                                Log.i("vehiculoParqueado",""+ vehiculoParqueado);


                            }catch (Exception e){
                                Log.v( "error", e.getMessage() );
                                Toast.makeText( getActivity(),"Escanea nuevamente", Toast.LENGTH_LONG ).show();
                            }

                            // verificamos que el token anterior no se igual al actual
                            // esto es util para evitar multiples llamadas empleando el mismo token



                        }
                    });


                }
            }
        });

    }


    //posgres dialos sweetalert

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
