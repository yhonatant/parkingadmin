package com.parkinglot.parkingadmin.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.ui.historial.HistorialFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InicioFragment extends Fragment {


    Modelo modelo = Modelo.getInstance();

    LinearLayout layoutver;
    SweetAlertDialog pDialog;

    public static CajaFragment newInstance(String param1, String param2) {
        CajaFragment fragment = new CajaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inicio, container, false);

        layoutver = view.findViewById(R.id.layoutver);


        Date date = new Date();


        String fecha = modelo.formatUniversal.format(date);


        String empresaID ="";
        JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
        try {
            empresaID = roles.get("org_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String finalEmpresaID = empresaID;
        layoutver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadswet("Cargando Vehiculos...");
                //registro(correo,password2);

                try {


                    Call<Object> obj = MyApiAdapter.getApiService().getListarFechaApp(finalEmpresaID,"2021-06-16");
                    obj.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {


                            Log.v("Resposnse", "" + response.body());

                            String jsonData = response.body().toString();




                            try {
                                JSONArray myJson = new JSONArray(jsonData);


                                if(myJson != null){
                                    modelo.listVehiculoSalidaRetrofits.clear();
                                    for (int i =0; i < myJson.length(); i++){

                                        Log.v("",""+myJson.get(i) );

                                        modelo.listVehiculoSalidaRetrofits.add(new VehiculoSalidaRetrofit((JSONObject) myJson.get(i)));
                                    }

                                }

                              int cant=   modelo.listVehiculoSalidaRetrofits.size();

                                Fragment fragment  = new ListadoVehiculos();
                                loadFragment(fragment);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            hideDialog();

                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Log.v("response t", t.getMessage());
                            hideDialog();
                            // alerta("Guardar Datos","Desea recordar los datos de ingreso para este dispositivo.");
                        }
                    });
                }catch (Exception e){
                    String error =  e.getMessage();
                    Log.v("Error", e.getLocalizedMessage());
                }

            }
        });
        return view;
    }


    //posgres dialos sweetalert

    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        } catch (Exception e) {

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}