package com.parkinglot.parkingadmin.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.adapter.ListVehiculoAdapter;
import com.parkinglot.parkingadmin.adapter.ListadoVehiculoAdapter;
import com.parkinglot.parkingadmin.model.Modelo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListadoVehiculos extends Fragment {


    Modelo modelo = Modelo.getInstance();

    LinearLayout layoutver;

    ListadoVehiculoAdapter listVehiculoAdapter;
    RecyclerView recyclerView;

    public static ListadoVehiculos newInstance(String param1, String param2) {
        ListadoVehiculos fragment = new ListadoVehiculos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listado_vehiculos, container, false);

        layoutver = view.findViewById(R.id.layoutver);
        final SearchView search2 = (SearchView) view.findViewById(R.id.search_view);
        recyclerView = view.findViewById(R.id.recycler_view2);
        recyclerView.setNestedScrollingEnabled(true);



        listVehiculoAdapter = new ListadoVehiculoAdapter(getActivity(), modelo.listVehiculoSalidaRetrofits);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listVehiculoAdapter);
        
        return view;
    }


}