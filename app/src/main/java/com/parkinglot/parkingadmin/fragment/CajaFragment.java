package com.parkinglot.parkingadmin.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.view.SalidaVehiculo;

import java.io.IOException;


public class CajaFragment extends Fragment {


    Modelo modelo = Modelo.getInstance();



    public static CajaFragment newInstance(String param1, String param2) {
        CajaFragment fragment = new CajaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_caja, container, false);


        return view;
    }

}