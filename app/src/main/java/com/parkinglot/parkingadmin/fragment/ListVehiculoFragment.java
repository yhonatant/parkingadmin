package com.parkinglot.parkingadmin.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.Retrofit.Configuracion.MyApiAdapter;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.adapter.ListVehiculoAdapter;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Comandos.ComandoListaVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;
import com.parkinglot.parkingadmin.view.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListVehiculoFragment extends Fragment implements ComandoListaVehiculoParqueado.OnValidarListaVehiculoParqueadoChangeListener {


    Modelo modelo = Modelo.getInstance();
    ComandoListaVehiculoParqueado comandoListaVehiculoParqueado;
    Utility utility;
    SweetAlertDialog pDialog;
    private static int splashTimeOut=5000;
    ListVehiculoAdapter listVehiculoAdapter;
    RecyclerView recyclerView;



    LinearLayout layoutcamara;
    LinearLayout layoutmanual;


    public ListVehiculoFragment() {
        // Required empty public constructor
    }

    public static ListVehiculoFragment newInstance(String param1, String param2) {
        ListVehiculoFragment fragment = new ListVehiculoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_vehiculo, container, false);


        comandoListaVehiculoParqueado = new ComandoListaVehiculoParqueado( this );

        final SearchView search2 = (SearchView) view.findViewById(R.id.search_view);
        recyclerView = view.findViewById(R.id.recycler_view2);
        recyclerView.setNestedScrollingEnabled(true);


        layoutcamara = (LinearLayout) view.findViewById(R.id.layoutcamara);
        layoutmanual = (LinearLayout) view.findViewById(R.id.layoutmanual);


        utility = new Utility();

        if (utility.estado(getActivity())) {
            loadswet("Cargando información...");
            new Handler().postDelayed( new Runnable() {
                @Override
                public void run() {

                    comandoListaVehiculoParqueado.getListaVhiculoParqueado();

                }
            },splashTimeOut);

        }else{
            alerta("Sin Internet","Valide la conexión a internet");
        }




       /* search2.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String queryString) {


                listVehiculoAdapter.getFilter().filter(queryString);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String queryString) {

                listVehiculoAdapter.getFilter().filter(queryString);

                return false;
            }
        });

*/




        layoutmanual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                    Date date = new Date();
                    String fecha = modelo.formatUniversal.format(date);


                    String empresaID ="";
                    JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();
                    try {
                        empresaID = roles.get("org_id").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String finalEmpresaID = empresaID;

                    loadswet("Cargando Vehiculos...");
                    //registro(correo,password2);

                    try {


                        Call<Object> obj = null;

                        if(modelo.optionTapas.equals("caja")){
                            obj =  MyApiAdapter.getApiService().getListarPorSalida(finalEmpresaID);
                        }else{
                            obj =  MyApiAdapter.getApiService().getListarFechaApp(finalEmpresaID,"2021-06-16");
                        }


                        obj.enqueue(new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {


                                Log.v("Resposnse", "" + response.body());

                                String jsonData = response.body().toString();




                                try {
                                    JSONArray myJson = new JSONArray(jsonData);

                                    modelo.listVehiculoSalidaRetrofits.clear();
                                    if(myJson != null){

                                        for (int i =0; i < myJson.length(); i++){

                                            Log.v("",""+myJson.get(i) );

                                            modelo.listVehiculoSalidaRetrofits.add(new VehiculoSalidaRetrofit((JSONObject) myJson.get(i)));
                                        }

                                    }

                                    int cant=   modelo.listVehiculoSalidaRetrofits.size();

                                    Fragment fragment  = new ListadoVehiculos();
                                    loadFragment(fragment);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                hideDialog();

                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                Log.v("response t", t.getMessage());
                                hideDialog();
                                // alerta("Guardar Datos","Desea recordar los datos de ingreso para este dispositivo.");
                            }
                        });
                    }catch (Exception e){
                        String error =  e.getMessage();
                        Log.v("Error", e.getLocalizedMessage());
                    }
                    //Fragment fragment  = new HistorialFragment();
                    //loadFragment(fragment);


            }
        });


        return view;
    }




    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }


    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor( Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }

    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    @Override
    public void validandoListaVehiculoParqueadoOK() {

        int cantidad = modelo.listVehiculoParqueado.size();
        Log.v("cantidad", ""+cantidad);
      try {

          if(cantidad > 0){

              listVehiculoAdapter = new ListVehiculoAdapter(getActivity(), modelo.listVehiculoParqueado);
              RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
              recyclerView.setLayoutManager(layoutManager);
              recyclerView.setAdapter(listVehiculoAdapter);
          }
          hideDialog();
      }catch (Exception e){
          Log.v( "Error", e.getMessage() );
          hideDialog();
      }
    }

    @Override
    public void validandoListaVehiculoParqueadoError() {
    }





    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



}
