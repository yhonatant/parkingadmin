package com.parkinglot.parkingadmin.model.Clases;

public class Convenio {
    String key;
    String nombre;

    public Convenio(){}

    public Convenio(String key, String nombre) {
        this.key = key;
        this.nombre = nombre;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
