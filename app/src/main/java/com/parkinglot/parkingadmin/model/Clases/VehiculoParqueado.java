package com.parkinglot.parkingadmin.model.Clases;

public class VehiculoParqueado {

    String key;
    String nombreParqueadero;
    String placa;
    String fecha;
    String estado;
    Long timestamp;
    String uidParqueadero;

    public VehiculoParqueado(){}

    public VehiculoParqueado(String key, String nombreParqueadero, String placa, String fecha, String estado, Long timestamp, String uidParqueadero) {
        this.key = key;
        this.nombreParqueadero = nombreParqueadero;
        this.placa = placa;
        this.fecha = fecha;
        this.estado = estado;
        this.timestamp = timestamp;
        this.uidParqueadero = uidParqueadero;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombreParqueadero() {
        return nombreParqueadero;
    }

    public void setNombreParqueadero(String nombreParqueadero) {
        this.nombreParqueadero = nombreParqueadero;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUidParqueadero() {
        return uidParqueadero;
    }

    public void setUidParqueadero(String uidParqueadero) {
        this.uidParqueadero = uidParqueadero;
    }
}
