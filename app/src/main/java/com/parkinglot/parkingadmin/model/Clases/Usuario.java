package com.parkinglot.parkingadmin.model.Clases;

import java.util.ArrayList;

public class Usuario {

    private  String key;
    private  String nombre;
    private  String apellido;
    private  String celular;
    private  String correo;
    private  String pasString;
    private  String token;
    private  double latitud;
    private  double longitud;
    private  boolean estado;
    private  String rol;


    public Usuario(){}

    public Usuario(String key, String nombre, String apellido, String celular, String correo, String pasString, String token, double latitud, double longitud, boolean estado, String rol) {
        this.key = key;
        this.nombre = nombre;
        this.apellido = apellido;
        this.celular = celular;
        this.correo = correo;
        this.pasString = pasString;
        this.token = token;
        this.latitud = latitud;
        this.longitud = longitud;
        this.estado = estado;
        this.rol = rol;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPasString() {
        return pasString;
    }

    public void setPasString(String pasString) {
        this.pasString = pasString;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
