package com.parkinglot.parkingadmin.model.Comandos;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.parkinglot.parkingadmin.model.Clases.Parqueadero;
import com.parkinglot.parkingadmin.model.Clases.Usuario;
import com.parkinglot.parkingadmin.model.Modelo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComandoParqueadero {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    //interface del listener de la actividad interesada
    private OnParqueaderosChangeListener mListener;

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnParqueaderosChangeListener {


        void cargoValidarCorreoFirebase();
        void cargoValidarCorreoFirebaseEroor();
        void  errorCreacionParqueadero();
        void setParqueaderoListener();
        void errorSetParqueadero();
        void setParueaderoListener();
        void cargoParqueadero();
        void addParueaderoListener();


    }

    public ComandoParqueadero(OnParqueaderosChangeListener mListener){

        this.mListener = mListener;

    }


    public  void registroParqueadero(Parqueadero p){

        DatabaseReference memoReference = FirebaseDatabase.getInstance().getReference();
        String key = memoReference.push().getKey();
        final DatabaseReference ref = database.getReference("Parqueadero/"+key);//ruta path


        Map<String, Object> parqueadero = new HashMap<String, Object>();

        parqueadero.put("nombreParqueadero", p.getNombreParqueadero());
        parqueadero.put("estado", p.getEstado());
        parqueadero.put("lat", p.getLat());
        parqueadero.put("long", p.getLon());
        parqueadero.put("horarioInicio", p.getHorarioInicio());
        parqueadero.put("horarioFin", p.getHorarioFin());
        parqueadero.put("precio", p.getPrecio());
        parqueadero.put("cupos", p.getCupos());
        parqueadero.put("disponible", p.getDisponibles());
        parqueadero.put("tipoMoneda", p.getTipoMoneda());
        parqueadero.put("tipoMoneda", p.getTipoMoneda());
        parqueadero.put("formaDeCobro",p.getformaDeCobro());
        parqueadero.put("timestamp", ServerValue.TIMESTAMP);



        ref.setValue(parqueadero, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.setParqueaderoListener();
                    return;
                } else {
                    mListener.errorSetParqueadero();
                }
            }
        });



    }




    public void getParqueadero(){

        modelo = Modelo.getInstance();

        DatabaseReference ref = database.getReference("Parqueadero/");//ruta path
        //se crea un query filtrado por el id del conductor
        Query query = ref.orderByChild("uid").equalTo(modelo.uid);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapchildres) {

                try{

                    for (DataSnapshot snap : snapchildres.getChildren()) {
                        boolean estado = (boolean) snap.child( "estado" ).getValue();
                        double lat = Double.parseDouble( snap.child( "lat" ).getValue().toString() );
                        double lon = Double.parseDouble( snap.child( "long" ).getValue().toString() );
                        double precio = Double.parseDouble( snap.child( "precio" ).getValue().toString() );
                        int cupos = Integer.parseInt( snap.child( "cupos" ).getValue().toString() );
                        int disponible = Integer.parseInt( snap.child( "disponible" ).getValue().toString() );

                        modelo.uidParqueadero = snap.getKey();
                        modelo.parqueadero.setKey( snap.getKey() );
                        modelo.parqueadero.setNombreParqueadero( snap.child( "nombreParqueadero" ).getValue().toString() );
                        modelo.parqueadero.setEstado( estado );
                        modelo.parqueadero.setLat( lat );
                        modelo.parqueadero.setLon( lon );
                        modelo.parqueadero.setHorarioInicio( snap.child( "horarioInicio" ).getValue().toString() );
                        modelo.parqueadero.setHorarioFin( snap.child( "horarioFin" ).getValue().toString() );
                        modelo.parqueadero.setPrecio( precio );
                        modelo.parqueadero.setCupos( cupos );
                        modelo.parqueadero.setDisponibles( disponible );
                        modelo.parqueadero.setTipoMoneda( snap.child( "tipoMoneda" ).getValue().toString() );
                        modelo.parqueadero.setformaDeCobro( snap.child( "formaDeCobro" ).getValue().toString() );
                        modelo.parqueadero.setUid( snap.child( "uid" ).getValue().toString() );

                    }
                    mListener.cargoParqueadero();
                }catch (Exception e){
                    Log.v( "Error", e.getMessage() );
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mListener.setParqueaderoListener();
            }
        });

    }



    public  void updateParqueadero(String nombreParqueadero,String horarioInicio, String horarioFin, double precio, int cupos, int disponibles,String tipoMoneda, String formaDeCobro){

        final DatabaseReference ref = database.getReference("Parqueadero/"+modelo.uidParqueadero+"/");//ruta path

        Map<String, Object> enviarRegistroUsuario = new HashMap<String, Object>();

        enviarRegistroUsuario.put("nombreParqueadero", nombreParqueadero);
        enviarRegistroUsuario.put("horarioInicio", horarioInicio);
        enviarRegistroUsuario.put("horarioFin", horarioFin);
        enviarRegistroUsuario.put("precio", precio);
        enviarRegistroUsuario.put("cupos", cupos);
        enviarRegistroUsuario.put("disponible", disponibles);
        enviarRegistroUsuario.put("tipoMoneda", tipoMoneda);
        enviarRegistroUsuario.put("formaDeCobro", formaDeCobro);



        ref.updateChildren(enviarRegistroUsuario, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.setParueaderoListener();
                    return;
                } else {
                    mListener.errorSetParqueadero();
                }
            }
        });
    }



    public  void registrarVehiculo(String placa,String fecha, String nombreParqueadero){

        DatabaseReference memoReference = FirebaseDatabase.getInstance().getReference();
        String key = memoReference.push().getKey();
        final DatabaseReference ref = database.getReference("VehiculoParqueado/"+modelo.uidParqueadero+"/"+key);//ruta path

        Map<String, Object> enviarRegistroUsuario = new HashMap<String, Object>();

        enviarRegistroUsuario.put("uidParqueadero", modelo.uidParqueadero);
        enviarRegistroUsuario.put("nombreParqueadero", nombreParqueadero);
        enviarRegistroUsuario.put("placa", placa);
        enviarRegistroUsuario.put("fecha", fecha);
        enviarRegistroUsuario.put("estado", "parqueado");
        enviarRegistroUsuario.put("timestamp", ServerValue.TIMESTAMP);



        ref.updateChildren(enviarRegistroUsuario, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {

                    mListener.addParueaderoListener();
                    return;
                } else {
                    mListener.errorSetParqueadero();
                }
            }
        });
    }



    /**
     * Para evitar nullpointerExeptions
     */
    private static OnParqueaderosChangeListener sDummyCallbacks = new OnParqueaderosChangeListener()
    {



        @Override
        public void cargoValidarCorreoFirebase() {}

        @Override
        public void cargoValidarCorreoFirebaseEroor(){}

        @Override
        public void setParqueaderoListener() {}

        @Override
        public void errorSetParqueadero() {}

        @Override
        public void setParueaderoListener() {}

        @Override
        public void errorCreacionParqueadero() {}

        @Override
        public void cargoParqueadero() {}

        @Override
        public void addParueaderoListener() {}
    };
}
