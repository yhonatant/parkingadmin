package com.parkinglot.parkingadmin.model;


import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.Contexto;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.UsuarioParqueaderoRetrofit;
import com.parkinglot.parkingadmin.Retrofit.ModeloRetrofit.VehiculoSalidaRetrofit;
import com.parkinglot.parkingadmin.Retrofit.viewRetrofit.ContextoListRetrofit;
import com.parkinglot.parkingadmin.model.Clases.ClassTerminosYCondiciones;
import com.parkinglot.parkingadmin.model.Clases.Convenio;
import com.parkinglot.parkingadmin.model.Clases.HistoricoVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Clases.Parqueadero;
import com.parkinglot.parkingadmin.model.Clases.Usuario;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class Modelo {
    public static final Modelo ourInstance = new Modelo();
    public String uid = "";
    public String uidParqueadero="";
    public String tipo ="";
    public String terminosycondiciones = "";
    public double latitud;//4.81321;
    public double longitud;// -75.6946;
    public GoogleMap mMap;
    public String token = "";
    public String optionTapas="";
    public String _salisa ="";
    public String formadepago="";
    public String voletafactura = "";

    public Contexto contexto = new Contexto();

    public static Modelo getInstance() {
        return ourInstance;
    }

    public Modelo() {
    }


    public ClassTerminosYCondiciones classTerminosYCondiciones = new ClassTerminosYCondiciones();
    public String tipoLogin = "";
    public Parqueadero parqueadero = new Parqueadero();
    public Usuario usuario = new Usuario();
    public UsuarioParqueaderoRetrofit usuarioParqueaderoRetrofit = new UsuarioParqueaderoRetrofit();
    public VehiculoParqueado vehiculoParqueado = new VehiculoParqueado();

    public ArrayList<VehiculoParqueado>listVehiculoParqueado = new ArrayList<VehiculoParqueado>();
    public ArrayList<HistoricoVehiculoParqueado>listHistoricoVehiculoParqueado = new ArrayList<HistoricoVehiculoParqueado>();
    public ArrayList<Convenio>listConvenio = new ArrayList<Convenio>();
    public ArrayList<ContextoListRetrofit> contextoListRetrofits = new ArrayList<ContextoListRetrofit>();

    //version app
    public String versionapp ="";
    public DecimalFormat decimales = new DecimalFormat("#.##");

    //datos date


    //Date date = new Date();
    //Caso 1: obtener la hora y salida por pantalla con formato:
    public DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
    //System.out.println("Hora: "+hourFormat.format(date));
    //Caso 2: obtener la fecha y salida por pantalla con formato:
    public DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public DateFormat formatUniversal = new SimpleDateFormat("yyyy-MM-dd");
    //System.out.println("Fecha: "+dateFormat.format(date));
    //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
    public DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    //System.out.println("Hora y fecha: "+hourdateFormat.format(date));

    public DateFormat _formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public DateFormat formatterz = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    //Devuelve true si la cadena que llega es un numero decimal, false en caso contrario
    public boolean esDecimal(String cad)
    {
        try
        {
            Double.parseDouble(cad);
            return true;
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
    }


    //metodo para mostrar una sola  orden segun el id de la orden
    public VehiculoParqueado getOrden(String idOrden){
        Iterator<VehiculoParqueado> iterator = listVehiculoParqueado.iterator();
        while (iterator.hasNext()) {
            VehiculoParqueado orden = iterator.next();
            if (orden.getKey().equals(idOrden)) {
                return  orden;
            }
        }
        return null;
    }

    public void eliminarOrden(String idOrden){
        Iterator<VehiculoParqueado> iterator = listVehiculoParqueado.iterator();
        while (iterator.hasNext()){
            VehiculoParqueado  orden = iterator.next();
            if(orden.getKey().equals(idOrden)){
                iterator.remove();
            }
        }
    }


    //retrofit
    public ArrayList<VehiculoSalidaRetrofit>listVehiculoSalidaRetrofits = new ArrayList<VehiculoSalidaRetrofit>();
    public VehiculoSalidaRetrofit vehiculoSalidaRetrofits = new VehiculoSalidaRetrofit();

    /**
     * Permite convertir un String en fecha (Date).
     * @param fecha Cadena de fecha dd/MM/yyyy
     * @return Objeto Date
     */
    public static Date ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        }
        catch (ParseException ex)
        {
            System.out.println(ex);
        }
        return fechaDate;
    }



    public static Date getDifferenceBetwenDates(Date dateInicio, Date dateFinal) {
        long milliseconds = dateFinal.getTime() - dateInicio.getTime();
        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.SECOND, seconds);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }


}
