package com.parkinglot.parkingadmin.model.Comandos;



import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.client.annotations.Nullable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.parkinglot.parkingadmin.model.Clases.Convenio;
import com.parkinglot.parkingadmin.model.Clases.HistoricoVehiculoParqueado;
import com.parkinglot.parkingadmin.model.Clases.VehiculoParqueado;
import com.parkinglot.parkingadmin.model.Modelo;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by tacto on 2/10/17.
 */

public class ComandoConvenio {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    //interface del listener de la actividad interesada
    private OnValidarListaVehiculoParqueadoChangeListener mListener;

    public ComandoConvenio(OnValidarListaVehiculoParqueadoChangeListener mListener){

        this.mListener = mListener;

    }

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnValidarListaVehiculoParqueadoChangeListener {

        void validandoConvenioOK();
        void validandConvenioError();


    }




    public void  getListaConvenio(){
        //preguntasFrecuentes
        modelo.listConvenio.clear();
        DatabaseReference ref = database.getReference("convenio/");//ruta path

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snFav, @Nullable String s) {
                Convenio convenio = new Convenio();


                convenio.setKey(snFav.getKey());
                convenio.setNombre(snFav.child("nombre").getValue().toString());




                modelo.listConvenio.add(convenio);

                mListener.validandoConvenioOK();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.v("dataSnapshot", "1");
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Log.v("dataSnapshot", "2");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.v("dataSnapshot", "3");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.v("dataSnapshot", "4");
            }
        });


    }





    /**
     * Para evitar nullpointerExeptions
     */
    private static OnValidarListaVehiculoParqueadoChangeListener sDummyCallbacks = new OnValidarListaVehiculoParqueadoChangeListener()
    {
        @Override
        public void validandoConvenioOK()
        {}


        @Override
        public void validandConvenioError()
        {}





    };

}