package com.parkinglot.parkingadmin.ui.parqueadero;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.model.Clases.Timedialog;
import com.parkinglot.parkingadmin.model.Comandos.ComandoParqueadero;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.utility.Utility;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ParqueaderoFragment extends Fragment implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,
        ComandoParqueadero.OnParqueaderosChangeListener {

    private ParqueaderoViewModel slideshowViewModel;
    Modelo modelo = Modelo.getInstance();
    EditText nameparkinlot, starttime, endttime, formofpayment, price, quotas, currency;
    int setHora = 0;
    Button next;
    Utility utility;
    SweetAlertDialog pDialog;
    ComandoParqueadero comandoParqueadero;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of( this ).get( ParqueaderoViewModel.class );
        View root = inflater.inflate( R.layout.fragment_parqueadero, container, false );

        nameparkinlot = (EditText) root.findViewById( R.id.nameparkinlot );
        starttime = (EditText) root.findViewById( R.id.starttime );
        endttime = (EditText) root.findViewById( R.id.endttime );
        formofpayment = (EditText) root.findViewById( R.id.formofpayment );
        price = (EditText) root.findViewById( R.id.price );
        quotas = (EditText) root.findViewById( R.id.quotas );
        currency = (EditText) root.findViewById( R.id.currency );
        next = (Button) root.findViewById( R.id.next );

        utility = new Utility();

        nameparkinlot.setText( modelo.parqueadero.getNombreParqueadero() );
        starttime.setText( modelo.parqueadero.getHorarioInicio() );
        endttime.setText( modelo.parqueadero.getHorarioFin() );
        formofpayment.setText( modelo.parqueadero.getformaDeCobro() );
        price.setText( ""+modelo.parqueadero.getPrecio() );
        quotas.setText( ""+modelo.parqueadero.getDisponibles());
        currency.setText( modelo.parqueadero.getTipoMoneda() );
        comandoParqueadero  = new ComandoParqueadero( this );

        //starttime
        starttime.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHora = 1;
                customTimePickerDialog(view);
            }
        } );

        endttime.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHora = 2;
                customTimePickerDialog(view);
            }
        } );


        currency.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] singleChoiceItems = getResources().getStringArray( R.array.arraycurrency );
                int itemSelected = 0;
                new AlertDialog.Builder( getActivity() )
                        .setTitle( "Selecione el tipo de moneda" )
                        .setSingleChoiceItems( singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                                currency.setText( singleChoiceItems[selectedIndex] );
                            }
                        } )
                        .setPositiveButton( "Ok", null )
                        .setNegativeButton( "Cancel", null )
                        .show();
            }
        } );

        formofpayment.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] singleChoiceItems = getResources().getStringArray( R.array.formadecobro );
                int itemSelected = 0;
                new AlertDialog.Builder( getActivity() )
                        .setTitle( "Selecione el tipo de fracción " )
                        .setSingleChoiceItems( singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                                formofpayment.setText( singleChoiceItems[selectedIndex] );
                            }
                        } )
                        .setPositiveButton( "Ok", null )
                        .setNegativeButton( "Cancel", null )
                        .show();
            }
        } );



        next.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameparkinlot.getText().toString().length() < 3) {
                    nameparkinlot.setError( "nombre del parqueadero muy corta" );
                } else if (starttime.getText().toString().length() < 3) {
                    starttime.setError( "Fecha inicio  requerido" );
                } else if (endttime.getText().toString().length() < 3) {
                    endttime.setError( "Fecha fin requerido" );
                } else if (price.getText().toString().length() < 2) {
                    price.setError( "El precio es requerido" );
                } else if (currency.getText().toString().length() < 2) {
                    currency.setError( "Valor moneda es requerido" );
                } else if (formofpayment.getText().toString().length() < 3) {
                    formofpayment.setError( "Forma de cobro es requerido" );
                } else if (quotas.getText().toString().length() < 2) {
                    quotas.setError( "Cupos es requerido" );
                } else {

                    if (utility.estado( getActivity() )) {
                        loadswet( "Validando la información..." );


                        String nombreParqueadero = nameparkinlot.getText().toString();
                        String horarioInicio = starttime.getText().toString();
                        String horarioFin = endttime.getText().toString();
                        double precio = Double.parseDouble( price.getText().toString() );
                        int cupos = Integer.parseInt( quotas.getText().toString() );
                        int disponibles = Integer.parseInt( quotas.getText().toString() );
                        String tipoMoneda = currency.getText().toString();
                        String formaDeCobro = formofpayment.getText().toString();


                        comandoParqueadero.updateParqueadero(nombreParqueadero,horarioInicio,horarioFin,precio,cupos,disponibles,tipoMoneda,formaDeCobro);

                    } else {
                        alerta( "Sin Internet", "Valide la conexión a internet" );
                    }

                }
            }
        } );

        return root;
    }



    public void loadswet(String text) {

        try {
            pDialog = new SweetAlertDialog( getActivity(), SweetAlertDialog.PROGRESS_TYPE );
            pDialog.getProgressHelper().setBarColor( Color.parseColor( "#A5DC86" ) );
            pDialog.setTitleText( text );
            pDialog.setCancelable( false );
            pDialog.show();

        } catch (Exception e) {

        }

    }
    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    //alerta
    public void alerta(String titulo, String decripcion) {
        new SweetAlertDialog( getActivity(), SweetAlertDialog.WARNING_TYPE )
                .setTitleText( titulo )
                .setContentText( decripcion )
                .setConfirmText( "Aceptar" )
                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                } )

                .show();
    }



    //picker



    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = "You picked the following time: " + hourString + "h" + minuteString + "m" + secondString + "s";
        starttime.setText(time);
    }




    private void customTimePickerDialog(View view) {

        Timedialog timedialog = new Timedialog();

        timedialog.get(view);
        android.app.FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();

        timedialog.show(fragmentTransaction,"TimePicker");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        starttime.setText(date);
    }

    @Override
    public void cargoValidarCorreoFirebase() {

    }

    @Override
    public void cargoValidarCorreoFirebaseEroor() {

    }

    @Override
    public void errorCreacionParqueadero() {
        hideDialog();
    }

    @Override
    public void setParqueaderoListener() {

    }

    @Override
    public void errorSetParqueadero() {
        hideDialog();
    }

    @Override
    public void setParueaderoListener() {

        hideDialog();

        alerta( "Actualizacón","Datos actualizdos con éxito" );
    }

    @Override
    public void cargoParqueadero() {

    }

    @Override
    public void addParueaderoListener() {

    }
}