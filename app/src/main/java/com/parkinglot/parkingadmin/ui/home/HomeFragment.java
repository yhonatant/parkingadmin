package com.parkinglot.parkingadmin.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parkinglot.parkingadmin.R;
import com.parkinglot.parkingadmin.fragment.InicioFragment;
import com.parkinglot.parkingadmin.fragment.LeerQRFragment;
import com.parkinglot.parkingadmin.fragment.ListVehiculoFragment;
import com.parkinglot.parkingadmin.fragment.RegistrarVehiculosFragment;
import com.parkinglot.parkingadmin.model.Modelo;
import com.parkinglot.parkingadmin.model.helper.BottomNavigationBehavior;
import com.parkinglot.parkingadmin.model.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class HomeFragment extends Fragment {

    Utility utility;
    Modelo modelo = Modelo.getInstance();
    private HomeViewModel homeViewModel;
    String es_entrada = "";
    String es_salida = "";
    String es_caja = "";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        // final TextView textView = root.findViewById( R.id.text_home );
      /*  homeViewModel.getText().observe( getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                //textView.setText( s );
            }
        } );*/

        BottomNavigationView navigation = root.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        // CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        //layoutParams.setBehavior(new BottomNavigationBehavior());

        // load the store fragment by default

        utility = new Utility();

        utility.checkPermissionCamera(getActivity());


        //retrofit roles
        JSONObject roles = modelo.usuarioParqueaderoRetrofit.getRoles();

        try {

            es_entrada = roles.get("es_entrada").toString();
            es_salida = roles.get("es_salida").toString();
            es_caja = roles.get("es_caja").toString();

            Log.v("roles", es_entrada);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        loadFragment(new InicioFragment());

        return root;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;


            switch (item.getItemId()) {


                case R.id.navigation_home:

                    modelo.optionTapas = "inicio";
                    fragment = new InicioFragment();
                    loadFragment(fragment);


                    return true;

                case R.id.navigation_caja:

                    if (es_caja.equals("Y")) {
                        modelo.optionTapas = "caja";
                        fragment = new ListVehiculoFragment();
                        loadFragment(fragment);
                    } else {
                        Toast.makeText(getContext(), "No tiene acceso a este modulo", Toast.LENGTH_LONG).show();
                    }

                    return true;
                case R.id.navigation_registro_vehiculo:

                    if (es_entrada.equals("Y")) {
                        modelo.optionTapas = "registro";
                        fragment = new RegistrarVehiculosFragment();
                        loadFragment(fragment);
                    } else {
                        Toast.makeText(getContext(), "No tiene acceso a este modulo", Toast.LENGTH_LONG).show();
                    }

                    return true;


                case R.id.navigation_leer_qr:

                    if (es_salida.equals("Y")) {
                        modelo.optionTapas = "salida";
                        fragment = new LeerQRFragment();
                        loadFragment(fragment);
                    } else {
                        Toast.makeText(getContext(), "No tiene acceso a este modulo", Toast.LENGTH_LONG).show();
                    }

                    return true;


            }

            return false;
        }
    };

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}